lib: config: id: layout: position: workspaces: emotes: let
  # Map workspaces and emotes to something like that:
  # "1" = "";
  # "2" = "";
  # "3" = "";
  formatIcons = lib.listToAttrs (
    lib.lists.imap0 (i: v: {
      name = toString v;
      value =
        if (lib.length emotes > i)
        then lib.elemAt emotes i
        else "";
    })
    workspaces
  );
  base = import ../ui/waybar/layout/${layout} {
    inherit config workspaces formatIcons;
  };
in
  {
    output = id;
    position = position;
    layer = "top";
    margin = "2 6 2 6";
    #spacing = 2;
  }
  // base
