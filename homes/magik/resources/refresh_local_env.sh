#!/bin/bash

# !!! A chaque lancement la version de ce fichier utilisée est l'ancienne !!!

echo ""
echo "Refreshing install files from isos UNRAID\'s share"
echo "=================================================="
rsync -avh --progress --delete /mnt/isos/media\ server\ install/scripts/ /appdata/scripts/
rsync -avh --progress --delete /mnt/isos/media\ server\ install/resources/ /appdata/resources/
rsync -avh --progress --delete /mnt/isos/media\ server\ install/doc/ /appdata/doc/
rsync -avh --progress --delete /mnt/isos/media\ server\ install/homebank/ /appdata/homebank/ #TODO

echo ""
echo "Copying docker compose file"
echo "==========================="
cp /mnt/isos/media\ server\ install/docker-compose*.yml /appdata/

echo ""
echo "Moving config from their resources folder to the final conf folder"
echo "=================================================================="
sudo rm /appdata/config/grafana/dashboards/*.json
sudo cp -r /appdata/resources/cutom_bash_profile.sh ~/.cutom_bash_profile
sudo cp -r /appdata/resources/auto-reloaded/* /appdata/config/
sudo chmod 754 /appdata/config/traefik/traefik.yml

echo ""
echo "Making scripts executable"
echo "========================="
sudo dos2unix ~/.cutom_bash_profile
sudo dos2unix /appdata/scripts/**/*.sh
sudo dos2unix /appdata/scripts/*.sh
chmod u+x /appdata/scripts/**/*.sh
chmod u+x /appdata/scripts/*.sh
chmod u+x /appdata/scripts/**/*.py
chmod u+x /appdata/scripts/*.py

source ~/.bashrc
