#!/bin/bash

# Setup backups cron
echo "20 4 1,15 * *   root     /appdata/scripts/backup.sh" >>/etc/crontab
echo "*  * *    * *   root     /appdata/scripts/test.sh" >>/etc/crontab

# Prepare for first run
mkdir -p /appdata
chown -R media:media /appdata
cp /shares/isos/media\ server\ install/resources/.env /appdata/.env
cat /appdata/.env
