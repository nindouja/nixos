{
  pkgs,
  config,
  ...
}: {
  config.nindouja = {
    user = {
      name = "Magik";
      email = "nindouja@proton.me";
    };

    appearance = {
      theme = "magikube";
    };

    bash.extraBashRc = ''
      KUBECONFIG=/home/${config.nindouja.user.username}/k3s.yaml
      if [ -f "$KUBECONFIG" ]; then
        export KUBECONFIG
      else
        echo "$KUBECONFIG file is not set. Copy it from /etc/rancher/..."
      fi
    '';

    l10n = {
      timezone = "Europe/Paris";
      languageLocale = "en_US.UTF-8";
      formatLocale = "fr_FR.UTF-8";
      keyboardLayout = "fr";
    };

    features = {
      docker = true;
      k3s = true;
      ssh = true;
      xdg = true;

      backups = {
        enable = true;
        dns = "ns5001772.ip-192-95-33.net";
      };
    };

    apps = {
      editor = "nano";
      kitty = true;
      starship = true;
      vscodium = true;
      firefox = true;
    };

    shares = {
      isos = {
        enable = true;
        useTag = true;
      };
      media = {
        enable = true;
        useTag = true;
      };
      public = {
        enable = true;
        useTag = true;
      };
      backup = {
        enable = true;
        useTag = true;
      };
      private = {
        enable = true;
        useTag = true;
      };
    };
  };
}
