{
  lib,
  pkgs,
  config,
  inputs,
  ...
}: let
  monitors = builtins.map (m: "${m.id}, ${m.width}x${m.height}@${m.fps}, ${m.posX}x${m.posY}, ${m.scale}, transform, ${m.transform}") config.nindouja.devices.monitors;
  workspaces = builtins.concatLists (builtins.map (m: builtins.map (ws: "${builtins.toString ws}, monitor:${m.id}") m.workspaces) config.nindouja.devices.monitors);
in {
  wayland.windowManager.hyprland = {
    settings = {
      general = {
        gaps_in = 4;
        gaps_out = 2;
        border_size = 3;
        resize_on_border = true;
        layout = "dwindle";
        "col.inactive_border" = lib.mkForce "rgb(${config.lib.stylix.colors.base06})";
        "col.active_border" = lib.mkForce "rgb(${config.lib.stylix.colors.base0B})";
      };

      animations = {
        enabled = true;
        bezier = [
          "pace,0.46, 1, 0.29, 0.99"
          "overshot,0.13,0.99,0.29,1.1"
          "md3_decel, 0.05, 0.7, 0.1, 1"
        ];
        animation = [
          "windowsIn,1,6,md3_decel,slide"
          "windowsOut,1,6,md3_decel,slide"
          "windowsMove,1,6,md3_decel,slide"
          "fade,1,10,md3_decel"
          "workspaces,1,9,md3_decel,slide"
          "workspaces, 1, 6, default"
          "specialWorkspace,1,8,md3_decel,slide"
          "border,1,10,md3_decel"
        ];
      };

      decoration = {
        rounding = 10;
        active_opacity = 0.95;
        inactive_opacity = 0.9;
        fullscreen_opacity = 1;
        blur = {
          enabled = true;
          brightness = 0.59;
          contrast = 1;
          new_optimizations = true;
          passes = 2;
          size = 9;
        };
      };

      dwindle = {
        pseudotile = true; # enable pseudotiling on dwindle
        force_split = 0;
        preserve_split = true;
        default_split_ratio = 1.0;
        special_scale_factor = 0.8;
        split_width_multiplier = 1.0;
        use_active_for_splits = true;
      };

      master = {
        mfact = 0.5;
        orientation = "right";
        special_scale_factor = 0.8;
      };

      windowrule = [
      ];

      windowrulev2 = [
        # Apps pined to workspace by defaults
        # 1 - Web browsers
        "workspace 1,class:(firefox)"
        # 2 - Games
        "workspace 2,class:(steam)"
        "workspace 2,title:(Steam)"
        "workspace 2,title:(lutris)"
        "workspace 2,title:(upc.exe)"
        "workspace 2,title:(explorer.exe)"
        "fullscreen,class:(steam_app_.*)"
        # 3 - IDEs
        "workspace 3,initialtitle:(VSCodium)"
        "workspace 3,initialtitle:(obsidian)"
        "workspace 3,initialtitle:(jetbrains-idea)"
        "workspace 3,initialtitle:(jetbrains-rustrover)"
        # 5 - Multimedia
        "workspace 5,class:(plexmediaplayer)"
        "fullscreen,class:(plexmediaplayer)"
        # 7 - Messaging
        "workspace 7,class:(WebCord)"
        "workspace 7,class:(Element)"
        # 8 - Terminals
        "workspace 8,class:(kitty)"
        "workspace 8,class:(Alacritty)"
        "workspace 8,class:(org.gnome.Console)"

        # Picture in picture (firefox)
        "keepaspectratio,class:^(firefox)$,title:^(Picture-in-Picture)$"
        "noborder,class:^(firefox)$,title:^(Picture-in-Picture)$"
        "fullscreenstate 2,class:^(firefox)$,title:^(Firefox)$"
        "fullscreenstate 2,class:^(firefox)$,title:^(Picture-in-Picture)$"
        "pin,class:^(firefox)$,title:^(Firefox)$"
        "pin,class:^(firefox)$,title:^(Picture-in-Picture)$"
        "float,class:^(firefox)$,title:^(Firefox)$"
        "float,class:^(firefox)$,title:^(Picture-in-Picture)$"
        "opacity 1.0,class:^(firefox)$,title:^(Picture-in-Picture)$"

        # Popups
        # TODO "float,class:^(lxqt-policykit-agent)$"
        # TODO "float,title:^(Authentication Required)$"
      ];

      exec-once = [
        "auto_fix" # Custom script to UI (waybar, swww)
        "ollama serve" # Start ollama api
      ];

      # FN keys dependent on host
      bindl = (
        if (config.nindouja.hostname == "GAMETOP")
        then [
          " , code:156, exec, hyprlock" #                                                            M4                     - Lock session
          #" , code:121, exec, amixer set Master toggle" #                                            Fn + F1                - Mute volume
          " , code:237, exec, brightnessctl -d ${config.nindouja.devices.keyboardBacklight} s 10%-" #Fn + F2                - Lower keyboard brightness
          " , code:238, exec, brightnessctl -d ${config.nindouja.devices.keyboardBacklight} s +10%" #Fn + F3                - Raise keaybpard brightness
          " , code:210, exec, echo 'Fn + F4'" #                                                      Fn + F4                - UNMAPPED
          " , code:211, exec, echo 'Fn + F5'" #                                                      Fn + F5                - UNMAPPED
          #" , code:, exec, echo 'Fn + F6'"                                                          Fn + F6                - HARDCODED
          #" , code:232, exec, brightnessctl -d ${config.nindouja.devices.screenBacklight} s 10%-" #  Fn + F7                - Lower screeen brightness
          #" , code:233, exec, brightnessctl -d ${config.nindouja.devices.screenBacklight} s +10%" #  Fn + F8                - Raise screeen brightness
          #" , code:, exec, echo 'Fn + F9'"                                                          Fn + F9                - HARDCODED
          " , code:199, exec, echo 'Fn + F10'" #                                                     Fn + F10               - UNMAPPED
          #" , code:, exec, echo 'Fn + F11'"                                                         Fn + F11               - HARDCODED
          #" , code:, exec, echo 'Fn + F12'"                                                         Fn + F12               - HARDCODED
        ]
        else []
      );

      bind = [
        # SUPER + IMPR ECRAN = cava overlay
        "SUPER, code:107, exec, kitty +kitten panel --edge=center --edge=bottom --lines=10 --class=cava_overlay cava"
        "SUPER SHIFT, code:107, exec, ps aux | grep 'cava_overlay' | awk '{print $2}' | xargs kill -9"

        # Remote sessions
        "SUPER, code:67, exec, kitty kitten ssh root@192.168.0.100" #                                      SUPER + F1             - SSH Unraid
        "SUPER, code:68, exec, kitty kitten ssh media@192.168.0.101" #                                     SUPER + F2             - SSH Vizbab
        "SUPER, code:69, exec, kitty kitten ssh magik@192.168.0.102" #                                     SUPER + F3             - SSH Magikube
        "SUPER, code:70, exec, kitty kitten ssh root@ns5001772.ip-192-95-33.net" #                         SUPER + F4             - SSH OVH Backups server

        # Common apps
        "SUPER SHIFT, S, exec, steam" #                                                                    SUPER + SHIFT + S             - Steam
        "SUPER SHIFT, K, exec, kitty -o font_size=12 --hold fastfetch" #                                   SUPER + SHIFT + K             - Kitty
        "SUPER SHIFT, L, exec, hgx" #                                                                      SUPER + SHIFT + L             - Consol
        "SUPER SHIFT, Z, exec, nautilus --new-window" #                                                    SUPER + SHIFT + Z             - File manager

        # Pass shortcuts to apps
        "SUPER,F10,pass,^(com\.obsproject\.Studio)$"
      ];
    };
  };

  home.packages = with pkgs; [
    (pkgs.writeShellScriptBin "auto_fix" ''
      notify-send "Refreshing UI" "NOW"

      refresh_bar
      refresh_bg

      sleep 2
      pypr reload
    '')
  ];
}
