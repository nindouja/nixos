{
  pkgs,
  config,
  ...
}: {
  config.nindouja = {
    user = {
      name = "Nindouja";
      email = "nindouja@proton.me";
    };

    appearance = {
      theme = import ../../ui/themes/neon.nix {inherit pkgs;};
      waybar.theme = "boxed";
      hyprpanel = {
        baseTheme = "rose_pine_moon";
        customTheme = "main";
      };
    };

    bash.extraBashRc = ''
      KUBECONFIG=/home/${config.nindouja.user.username}/k3s.yaml
      if [ -f "$KUBECONFIG" ]; then
        export KUBECONFIG
      else
        echo "$KUBECONFIG file is not set. Copy it from /etc/rancher/..."
      fi
    '';

    l10n = {
      timezone = "Europe/Paris";
      languageLocale = "en_US.UTF-8";
      formatLocale = "fr_FR.UTF-8";
      keyboardLayout = "fr";
    };

    features = {
      docker = true;
      vms = true;
      openrgb = true; # TODO configure openrgb
      ssh = true;
      xdg = true;
      ollama = true;
      minigames = true;
      minecraft = true;
      input-remapper = true;

      backups = {
        enable = true;
        dns = "ns5001772.ip-192-95-33.net";
      };
      proton-sync = {
        enable = true;
        username = "nindouja@proton.me";
      };
      music = true;
    };

    apps = {
      editor = "nvim";
      browser = "firefox";
      kitty = true;
      kubectl = true;
      starship = true;
      steam = true;
      vscodium = true;
      cava = true;
      firefox = true;
      chromium = true;
      managarr = true;
      blender = true;
      discord = true;
      element = true;
      geogebra = true;
      gimp = true;
      inkscape = true;
      krita = true;
      obsidian = true;
      openscad = true;
      plex = true;
      vlc = true;
      obs = true;
      alpaca = true;
    };

    dev = {
      java = true;
      rust = true;
      go = true;
    };

    shares = {
      isos.enable = true;
      media.enable = true;
      public.enable = true;
      backup.enable = true;
      games.enable = true;
      private.enable = true;
      appdata.enable = true;
    };
  };
}
