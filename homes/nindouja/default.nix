{
  pkgs,
  config,
  pkgs-unstable,
  ...
}: {
  imports = [
    # User's specific stuff
    ./hyprland-config.nix
    ./stylix-config.nix
  ];
}
