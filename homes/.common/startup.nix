{
  lib,
  pkgs,
  config,
  ...
}: let
  script_path = ../${config.nindouja.user.username}/startup.sh;
in {
  config = lib.mkIf (builtins.pathExists script_path) {
    home.packages = with pkgs; [
      (pkgs.writeShellScriptBin "startup" (builtins.readFile script_path))
    ];
  };
}
