{config, ...}: {
  home.stateVersion = "23.11";
  home.username = config.nindouja.user.username;
  home.homeDirectory = "/home/" + config.nindouja.user.username;
}
