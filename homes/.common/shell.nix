{config, ...}: {
  programs = {
    bash = {
      enable = true;
      enableCompletion = true;
      bashrcExtra = config.nindouja.bash.extraBashRc;
    };
    direnv = {
      enable = true;
      enableBashIntegration = true;
      nix-direnv.enable = true;
    };
  };

  home.sessionVariables = {
    EDITOR = config.nindouja.apps.editor;
  };
}
