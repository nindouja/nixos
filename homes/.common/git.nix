{
  config,
  ninja-base-config,
  ...
}: {
  programs.git = {
    enable = true;
    userName = ninja-base-config.git.username;
    userEmail = ninja-base-config.git.email;
    extraConfig = {
      init.defaultBranch = "main";
    };
    aliases = {
      ci = "commit";
      co = "checkout";
      s = "status";
    };
  };
}
