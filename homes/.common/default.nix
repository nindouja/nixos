{
  pkgs,
  pkgs-unstable,
  inputs,
  ...
}: {
  imports = [
    ./base.nix
    ./git.nix
    ./shell.nix
    ./sops.nix
    ./startup.nix
  ];

  home.packages = (import ../../resources/must-have.nix {inherit pkgs pkgs-unstable inputs;}).must-have-pkgs;
  programs.bash.shellAliases = (import ../../resources/must-have.nix {inherit pkgs pkgs-unstable inputs;}).must-have-aliases;
}
