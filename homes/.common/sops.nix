{
  inputs,
  config,
  ...
}: let
  username = config.nindouja.user.username;
  secretsDirectory = builtins.toString inputs.ninja-secrets;
  homeDirectory = config.home.homeDirectory;
in {
  imports = [
    inputs.sops-nix.homeManagerModules.sops
  ];

  sops = {
    defaultSopsFile = "${secretsDirectory}/secrets.yaml";
    age = {
      # automatically import this user and the dev ssh keys as age keys
      sshKeyPaths = ["${homeDirectory}/.ssh/id_ed25519"];
      # this will use an age key that is expected to already be in the filesystem
      keyFile = "${homeDirectory}/.config/sops/age/key.txt";
      # generate a new key if the key specified above does not exist
      generateKey = true;
    };
  };
}
