{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.krita) {
    home.packages = with pkgs; [
      krita
    ];
  };
}
