{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.discord) {
    home.packages = with pkgs; [
      webcord
    ];
  };
}
