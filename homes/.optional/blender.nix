{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.blender) {
    home.packages = with pkgs; [
      blender
    ];
  };
}
