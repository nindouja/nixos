{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf config.nindouja.apps.alacritty {
    home.packages = with pkgs; [
      alacritty
    ];
    programs.alacritty.enable = true;
    programs.alacritty.settings = {
      window.opacity = lib.mkForce 0;
    };
  };
}
