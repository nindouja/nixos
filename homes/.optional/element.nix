{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.element) {
    home.packages = with pkgs; [
      element
    ];
  };
}
