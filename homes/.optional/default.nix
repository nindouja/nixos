{...}: {
  imports = [
    ./stylix

    ./alacritty.nix
    ./blender.nix
    ./cava.nix
    ./chromium.nix
    ./discord.nix
    ./element.nix
    ./firefox.nix
    ./geogebra.nix
    ./gimp.nix
    ./inkscape.nix
    ./kitty.nix
    ./krita.nix
    ./kubectl.nix
    ./managarr.nix
    ./minigames.nix
    ./minecraft.nix
    ./music.nix
    ./nixvim.nix
    ./obsidian.nix
    ./openscad.nix
    ./plex.nix
    ./proxmox.nix
    ./starship.nix
    ./steam.nix
    ./virtualisation.nix
    ./vlc.nix
    ./vscodium.nix
    ./xdg.nix
  ];
}
