{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.inkscape) {
    home.packages = with pkgs; [
      inkscape
    ];
  };
}
