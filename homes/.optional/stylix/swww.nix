{
  pkgs,
  stylix,
  config,
  ...
}: {
  home.packages = with pkgs; [
    swww
    (pkgs.writeShellScriptBin "refresh_bg" ''
      swww img ${config.stylix.image}
    '')
  ];

  home.sessionVariables = {
    SWWW_TRANSITION_FPS = "30";
    SWWW_TRANSITION_STEP = "10";
    SWWW_TRANSITION = "simple";
  };
}
