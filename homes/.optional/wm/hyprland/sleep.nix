{
  pkgs-unstable,
  config,
  ...
}: {
  home.packages = with pkgs-unstable; [
    hypridle

    (pkgs.writeShellScriptBin "night_mode" ''
      if [ ! -f /tmp/night_mode ]; then
        notify-send --app-name "Night mode" --urgency "low" "Night mode on '$1'"
        brightnessctl -sd ${config.nindouja.devices.screenBacklight} set 1%
        brightnessctl -sd ${config.nindouja.devices.keyboardBacklight} set 0%
        touch /tmp/night_mode
      else
        notify-send --app-name "Night mode" --urgency "low" "Night mode off '$1'"
        brightnessctl -rd ${config.nindouja.devices.screenBacklight}
        brightnessctl -rd ${config.nindouja.devices.keyboardBacklight}
        rm /tmp/night_mode
      fi
    '')
  ];

  home.file.".config/hypr/hypridle.conf".text =
    ''
      general {
        lock_cmd = pidof hyprlock || hyprlock       # avoid starting multiple hyprlock instances.
        before_sleep_cmd = loginctl lock-session    # lock before suspend.
        after_sleep_cmd = hyprctl dispatch dpms on  # to avoid having to press a key twice to turn on the display.
        inhibit_sleep=1
      }

      # This config will
      # Dim screen and keyboard after 1 min
      # Lock the session after 10 min
      # Turn off the screen after 11 min
      # Suspend system after 15 min (saved to RAM)
      # Hibernate system after 45 min (saved to SWAP)


      ######################
      ######## 1 min #######
      ######################
      # Dim screen
    ''
    + (
      if (config.nindouja.devices.screenBacklight != "none")
      then ''
        listener {
            timeout = 60
            on-timeout = brightnessctl -sd ${config.nindouja.devices.screenBacklight} set 1%
            on-resume =  brightnessctl -rd ${config.nindouja.devices.screenBacklight}
        }
      ''
      else ''
        # - no screenBacklight configured
      ''
    )
    + ''

      # Dim keyboard
    ''
    + (
      if (config.nindouja.devices.keyboardBacklight != "none")
      then ''
        listener {
            timeout = 60
            on-timeout = brightnessctl -sd ${config.nindouja.devices.keyboardBacklight} set 17%
            on-resume =  brightnessctl -rd ${config.nindouja.devices.keyboardBacklight}
        }
      ''
      else ''
        # - no keyboardBacklight configured
      ''
    )
    + ''

      ######################
      ####### 10 min #######
      ######################
      # Lock session
      listener {
          timeout = 600
          on-timeout = loginctl lock-session
      }

      ######################
      ####### 11 min #######
      ######################
      # Turn off screen
      listener {
          timeout = 660
          on-timeout = hyprctl dispatch dpms off
          on-resume = hyprctl dispatch dpms on
      }
    ''
    + ''
      # Turn off keyboard backlight
    ''
    + (
      if (config.nindouja.devices.keyboardBacklight != "none")
      then ''
        listener {
            timeout = 360
            on-timeout = brightnessctl -sd ${config.nindouja.devices.keyboardBacklight} set 0
            on-resume =  brightnessctl -rd ${config.nindouja.devices.keyboardBacklight}
        }
      ''
      else ''
        # - no keyboardBacklight configured
      ''
    )
    + ''

      ######################
      ####### 15 min #######
      ######################
      # Suspend pc then hibernate after 30min
      # The 30min is configured in /etc/systemd/sleep.conf (hibernation.nix)
      listener {
          timeout = 900
          on-timeout = systemctl suspend-then-hibernate
      }a
    '';
}
