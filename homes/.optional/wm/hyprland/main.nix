{
  pkgs,
  config,
  pkgs-unstable,
  ...
}: {
  home.packages = with pkgs-unstable; [
    pavucontrol # Volume controller
    playerctl # Media control CLI
    hyprpolkitagent # Elevation privilege provider for UIs  TODO: To add style a QT theme must be set ? I don't really now... ref:https://github.com/hyprwm/hyprpolkitagent/issues?q=is%3Aissue%20state%3Aclosed%20style

    libsForQt5.qt5ct #TODO added to try to correct above todo
    libsForQt5.qtstyleplugin-kvantum #TODO added to try to correct above todo

    xdg-utils
    xdg-desktop-portal
    xdg-desktop-portal-gtk
    xdg-desktop-portal-hyprland

    # A reusable script to kill all overlays (wofi and scratchpads)
    (pkgs.writeShellScriptBin "kill_overlays" ''
      pkill wofi || true
      pypr hide "*" || true
    '')
  ];

  #TODO added to try to correct above todo
  nixpkgs.config.qt5 = {
    enable = true;
    platformTheme = "qt5ct";
    style = {
      package = pkgs.utterly-nord-plasma;
      name = "Utterly Nord Plasma";
    };
  };

  home.sessionVariables = {
    NIXOS_OZONE_WL = "1"; # TODO
    ELECTRON_OZONE_PLATFORM_HINT = "auto";
  };
}
