{
  pkgs,
  config,
  pkgs-unstable,
  ...
}: let
  optional_scratchpad = name: (
    if config.nindouja.scratchpads.${name}
    then "class:(${name}),"
    else ""
  );
  optional_scratchpad_binding = name: code:
    if config.nindouja.scratchpads.${name}
    then "SUPER, ${code}, exec, pypr toggle ${name}"
    else "";
in {
  wayland.windowManager.hyprland = {
    settings = {
      # Mouse keybinds
      bindm = [
        "SUPER,mouse:272,movewindow" #                             SUPER + Mouse drag     - Move window
      ];

      # General keybinds (works on lockscreen)
      bindl = [
        # Power and session
        "SUPER SHIFT, code:119, exec, shutdown -r now" #           SUPER + MAJ + SUPPR    - Reboots
        "SUPER, code:119, exec, shutdown -P now" #                 SUPER + SUPPR          - Shutdowns
        "SUPER, code:115, exec, hyprctl dispatch exit" #           SUPER + FIN            - Log out

        # Volume toggles
        ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle" # Mute toggle
        ", XF86AudioMicMute, exec, wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle" # Mute microphone

        # Media keys FIXME ignore firefox is dynamic
        ", XF86AudioPlay, exec, playerctl play-pause --ignore-player=firefox.instance_1_11"
        ", XF86AudioNext, exec, playerctl next --ignore-player=firefox.instance_1_11"
        ", XF86AudioPrev, exec, playerctl previous --ignore-player=firefox.instance_1_11"
      ];

      # Repeat when held down (works on lockscreen)
      bindle = [
        # Volume Controls
        ", XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 2%+"
        ", XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 2%-"

        # Brightness Controls
        ", XF86MonBrightnessUp, exec, brightnessctl -d ${config.nindouja.devices.screenBacklight} s 2%+"
        ", XF86MonBrightnessDown, exec, brightnessctl -d ${config.nindouja.devices.screenBacklight} s 2%-"
      ];

      # On release keybinds
      bindr = [
        "SUPER, SUPER_L, exec, kill_overlays" #                    SUPER                  - Kill all scratchpad or wofi
      ];

      bind = [
        # Utilities
        ", XF86Search, exec, app_launcher"
        "SUPER, SPACE, exec, app_launcher" #                       SUPER + SPACE          - toggles app launcher
        "SUPER, V, exec, clipboard_manager" #                      SUPER + V              - toggles clipboard manager

        # Main
        "SUPER, O, exec, pypr expose" #                            SUPER + O              - Workspace overviw
        "SUPER, L, exec, hyprlock" #                               SUPER + L              - Lock the session
        "SUPER, R, exec, auto_fix" #                               SUPER + R              - Refresh UI elements (waybar, swww)
        "SUPER, W, killactive, " #                                 SUPER + W              - Close active window
        "SUPER, G, togglefloating, " #                             SUPER + G              - Toggles floating for active window
        "SUPER, F, fullscreen, " #                                 SUPER + F              - Toggles fullscreen for active window
        ''SUPER,P,exec,grim -g "$(slurp)" - | swappy -f -'' #      SUPER + P              - Screenshot part of the screen
        ''SUPER SHIFT,P,exec,grim - | swappy -f -'' #              SUPER + SHIFT + P      - Screenshot entire screen

        # Misc
        "SUPER , code:94, exec, night_mode 1" #                    SUPER + <               - Night mode
        "SUPER SHIFT , code:94, exec, night_mode 0" #              SUPER MAJ + <           - Night mode off

        # Focus
        "SUPER, left, movefocus, l" #                              SUPER +               - Move focus to left
        "SUPER, right, movefocus, r" #                             SUPER +               - Move focus to right
        "SUPER, up, movefocus, u" #                                SUPER +               - Move focus up
        "SUPER, down, movefocus, d" #                              SUPER +               - Move focus down
        "SUPER, TAB, focusmonitor, +1" #                           SUPER + TAB            - Move focus to next monitor
        "SUPER SHIFT, TAB, focusmonitor, -1" #                     SUPER + SHIFT + TAB    - Move focus to previous monitor

        # Move
        "SUPER SHIFT, left, movewindow, l" #                       SUPER +               - Move active windows to the left
        "SUPER SHIFT, right, movewindow, r" #                      SUPER +               - Move active windows to the right
        "SUPER SHIFT, up, movewindow, u" #                         SUPER +               - Move active windows up
        "SUPER SHIFT, down, movewindow, d" #                       SUPER +               - Move active windows down
        "SUPER SHIFT, ampersand, movetoworkspace, 1" #             SUPER + SHIFT + 1      - Move active window to workspace #1
        "SUPER SHIFT, eacute, movetoworkspace, 2" #                SUPER + SHIFT + 2      - Move active window to workspace #2
        "SUPER SHIFT, quotedbl, movetoworkspace, 3" #              SUPER + SHIFT + 3      - Move active window to workspace #3
        "SUPER SHIFT, apostrophe, movetoworkspace, 4" #            SUPER + SHIFT + 4      - Move active window to workspace #4
        "SUPER SHIFT, parenleft, movetoworkspace, 5" #             SUPER + SHIFT + 5      - Move active window to workspace #5
        "SUPER SHIFT, minus, movetoworkspace, 6" #                 SUPER + SHIFT + 6      - Move active window to workspace #6
        "SUPER SHIFT, egrave, movetoworkspace, 7" #                SUPER + SHIFT + 7      - Move active window to workspace #7
        "SUPER SHIFT, underscore, movetoworkspace, 8" #            SUPER + SHIFT + 8      - Move active window to workspace #8
        "SUPER SHIFT, ccedilla, movetoworkspace, 9" #              SUPER + SHIFT + 9      - Move active window to workspace #9
        "SUPER SHIFT, agrave, movetoworkspace, 10" #               SUPER + SHIFT + 0      - Move active window to workspace #10
        "SUPER SHIFT, code:20, movetoworkspace, 11" #              SUPER + SHIFT + )      - Move active window to workspace #11
        "SUPER SHIFT, code:21, movetoworkspace, 12" #              SUPER + SHIFT + =      - Move active window to workspace #12

        # Resize
        "SUPER SHIFT CTRL, right, resizeactive, 30 0" #            SUPER +               - Expand window size horizontaly
        "SUPER SHIFT CTRL, left, resizeactive, -30 0" #            SUPER +               - Reduce window size horizontaly
        "SUPER SHIFT CTRL, up, resizeactive, 0 -30" #              SUPER +               - Expand window size vertically
        "SUPER SHIFT CTRL, down, resizeactive, 0 30" #             SUPER +               - Reduce window size vertically

        # Workspaces
        "SUPER, mouse_up, workspace, e-1" #                        SUPER + MOUSE_UP       - Go to next workspace
        "SUPER, mouse_down, workspace, e+1" #                      SUPER + MOUSE_DOWN     - Go to previous workspace
        "SUPER, colon, workspace, e-1" #                           SUPER + :              - Go to next workspace
        "SUPER, code:61, workspace, e+1" #                         SUPER + !              - Go to previous workspace
        "SUPER, ampersand, workspace, 1" #                         SUPER + 1              - Go to workspace #1
        "SUPER, eacute, workspace, 2" #                            SUPER + 2              - Go to workspace #2
        "SUPER, quotedbl, workspace, 3" #                          SUPER + 3              - Go to workspace #3
        "SUPER, apostrophe, workspace, 4" #                        SUPER + 4              - Go to workspace #4
        "SUPER, parenleft, workspace, 5" #                         SUPER + 5              - Go to workspace #5
        "SUPER, minus, workspace, 6" #                             SUPER + 6              - Go to workspace #6
        "SUPER, egrave, workspace, 7" #                            SUPER + 7              - Go to workspace #7
        "SUPER, underscore, workspace, 8" #                        SUPER + 8              - Go to workspace #8
        "SUPER, ccedilla, workspace, 9" #                          SUPER + 9              - Go to workspace #9
        "SUPER, agrave, workspace, 10" #                           SUPER + 0              - Go to workspace #0

        # Scratchpads
        (optional_scratchpad_binding "dropterm" "A")
        (optional_scratchpad_binding "ninjaterm" "code:49")
        (optional_scratchpad_binding "music" "S")
        (optional_scratchpad_binding "volume" "code:35")
        (optional_scratchpad_binding "bluetooth" "code:34")
        (optional_scratchpad_binding "monitor" "code:51")
        (optional_scratchpad_binding "chatbot" "code:48")
      ];
    };
  };
}
