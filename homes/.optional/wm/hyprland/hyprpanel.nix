{
  lib,
  pkgs,
  inputs,
  config,
  ...
}: let
  inherit (lib) all;

  username = config.nindouja.user.username;
  weather_key_file = "weather_apikey.json";
  weathApiKeySopKey = "users/${config.nindouja.user.username}/weather_api_key";
  theme_variant =
    if config.nindouja.appearance.darkmode
    then config.nindouja.appearance.theme.dark
    else config.nindouja.appearance.theme.light;

  monitors = config.nindouja.devices.monitors;
  noPanel = all (monitor: (monitor.hyprpanel == "none")) monitors;
in {
  imports = [inputs.hyprpanel.homeManagerModules.hyprpanel];

  config = lib.mkIf (!noPanel) {
    sops = {
      secrets = {
        "${weathApiKeySopKey}" = {};
      };
      templates."${weather_key_file}" = {
        content = ''
          {
            "weather_api_key": "${config.sops.placeholder."${weathApiKeySopKey}"}"
          }
        '';
      };
    };

    programs.hyprpanel = {
      enable = true;
      overlay.enable = true;
      systemd.enable = true; # TODO deprectaed
      hyprland.enable = true;
      overwrite.enable = true;

      # Importing bars from each monitor config
      layout = {
        "bar.layouts" = builtins.listToAttrs (lib.imap0
          (index: monitor: let
            data = "";
          in {
            name = builtins.toString index;
            value = import ../../../../ui/hyprpanel/layout/${monitor.hyprpanel}.nix {
              inherit config;
            };
          })
          config.nindouja.devices.monitors);
      };

      theme = config.nindouja.appearance.hyprpanel.baseTheme;

      # Importing default theme and overrifind with one set in config.
      override = lib.mkMerge [
        (import ../../../../ui/hyprpanel/style/common.nix {
          inherit config;
        })
        (import ../../../../ui/hyprpanel/style/${config.nindouja.appearance.hyprpanel.customTheme}.nix {
          inherit config;
        })
      ];

      # Default settings
      settings = {
        dummy = true;
        scalingPriority = "hyprland";
        tear = false;
        terminal = "kitty";

        hyprpanel = {
          restartAgs = true;
          restartCommand = "${pkgs.hyprpanel}/bin/hyprpanel -q; ${pkgs.procps}/bin/pkill -u $USER -USR1 hyprpanel; ${pkgs.hyprpanel}/bin/hyprpanel";
        };

        notifications = {
          active_monitor = true;
          cache_actions = true;
          clearDelay = 100;
          displayedTotal = 8;
          monitor = 0;
          position = "top left";
          showActionsOnHover = false;
          timeout = 7005;
          # TODO not in nixos autoDismiss = false;
        };

        menus = {
          clock = {
            time = {
              hideSeconds = false;
              military = true;
            };
            weather = {
              enabled = true;
              interval = 60000;
              key = config.sops.templates."${weather_key_file}".path;
              location = "Lyon";
              unit = "metric";
            };
          };
          dashboard = {
            controls = {
              enabled = true;
            };
            # TODO
            directories = {
              enabled = false;
              left = {
                directory1 = {
                  command = "bash -c \"xdg-open $HOME/Downloads/\"";
                  label = "󰉍 Downloads";
                };
                directory2 = {
                  command = "bash -c \"xdg-open $HOME/Videos/\"";
                  label = "󰉏 Videos";
                };
                directory3 = {
                  command = "bash -c \"xdg-open $HOME/Projects/\"";
                  label = "󰚝 Projects";
                };
              };
              right = {
                directory1 = {
                  command = "bash -c \"xdg-open $HOME/Documents/\"";
                  label = "󱧶 Documents";
                };
                directory2 = {
                  command = "bash -c \"xdg-open $HOME/Pictures/\"";
                  label = "󰉏 Pictures";
                };
                directory3 = {
                  command = "bash -c \"xdg-open $HOME/\"";
                  label = "󱂵 Home";
                };
              };
            };
            powermenu = {
              avatar = {
                image = "$HOME/.face.icon"; # TODO
                name = "system";
              };
              confirmation = true;
              logout = "hyprctl dispatch exit";
              reboot = "systemctl reboot";
              shutdown = "systemctl poweroff";
              sleep = "systemctl suspend-then-hibernate";
            };
            # TODO
            shortcuts = {
              enabled = false;
              left = {
                shortcut1 = {
                  command = "microsoft-edge-stable";
                  icon = "󰇩";
                  tooltip = "Microsoft Edge";
                };
                shortcut2 = {
                  command = "spotify-launcher";
                  icon = "";
                  tooltip = "Spotify";
                };
                shortcut3 = {
                  command = "discord";
                  icon = "";
                  tooltip = "Discord";
                };
                shortcut4 = {
                  command = "rofi -show drun";
                  icon = "";
                  tooltip = "Search Apps";
                };
              };
              right = {
                shortcut1 = {
                  command = "sleep 0.5 && hyprpicker -a";
                  icon = "";
                  tooltip = "Color Picker";
                };
                shortcut3 = {
                  command = "bash -c \"/nix/store/l3s52iggy014p38vi1ri5mi4rjb4qggb-snapshot.sh\"";
                  icon = "󰄀";
                  tooltip = "Screenshot";
                };
              };
            };
            stats = {
              enable_gpu = true;
              enabled = true;
              interval = 2000;
            };
          };
          media = {
            displayTime = true;
            displayTimeTooltip = true;
            hideAlbum = false;
            hideAuthor = false;
            noMediaText = "ALLEZ LA !!";
          };
          power = {
            confirmation = true;
            logout = "hyprctl dispatch exit";
            lowBatteryNotification = false;
            lowBatteryNotificationText = "Your battery is running low ($POWER_LEVEL %).\\n\\nPlease plug in your charger.";
            lowBatteryNotificationTitle = "Warning: Low battery";
            lowBatteryThreshold = 20;
            reboot = "systemctl reboot";
            showLabel = true;
            shutdown = "systemctl poweroff";
            sleep = "systemctl suspend";
          };
          transition = "crossfade";
          transitionTime = 200;
          volume = {
            raiseMaximumVolume = true;
          };
        };

        bar = {
          autoHide = "fullscreen";
          scrollSpeed = 0;
          battery = {
            hideLabelWhenFull = true;
            label = true;
            middleClick = "";
            rightClick = "";
            scrollDown = "";
            scrollUp = "";
          };
          bluetooth = {
            label = true;
            middleClick = "";
            rightClick = "";
            scrollDown = "";
            scrollUp = "";
          };
          clock = {
            format = "%a %d %b  %H:%M:%S";
            icon = "󰸗";
            middleClick = "";
            rightClick = "";
            scrollDown = "";
            scrollUp = "";
            showIcon = true;
            showTime = true;
          };
          customModules = {
            cava = {
              autoSensitivity = true;
              barCharacters = [
                "▁"
                "▂"
                "▃"
                "▄"
                "▅"
                "▆"
                "▇"
                "█"
              ];
              bars = 10;
              channels = 2;
              framerate = 60;
              highCutoff = 10000;
              icon = "";
              leftClick = "";
              lowCutoff = 50;
              middleClick = "";
              noiseReduction = 0.77;
              rightClick = "";
              samplerate = 44100;
              scrollDown = "";
              scrollUp = "";
              showActiveOnly = false;
              showIcon = true;
              spaceCharacter = " ";
              stereo = false;
            };
            cpu = {
              icon = "";
              label = true;
              leftClick = "";
              middleClick = "";
              pollingInterval = 2000;
              rightClick = "";
              round = true;
              scrollDown = "";
              scrollUp = "";
            };
            cpuTemp = {
              icon = "";
              label = true;
              leftClick = "";
              middleClick = "";
              pollingInterval = 2000;
              rightClick = "";
              round = true;
              scrollDown = "";
              scrollUp = "";
              sensor = "temp1_input"; # FIXME https://hyprpanel.com/configuration/panel.html#cpu-temperature
              showUnit = true;
              unit = "metric";
            };
            hypridle = {
              label = true;
              middleClick = "";
              offIcon = "󰅶";
              offLabel = "";
              onIcon = "󰾪";
              onLabel = "";
              pollingInterval = 2000;
              rightClick = "";
              scrollDown = "";
              scrollUp = "";
            };
            hyprsunset = {
              label = true;
              middleClick = "";
              offIcon = "󰛨";
              offLabel = "";
              onIcon = "󱩌";
              onLabel = "";
              pollingInterval = 2000;
              rightClick = "";
              scrollDown = "brightnessctl -d none s 2%-";
              scrollUp = "brightnessctl -d none s 2%+";
              temperature = "2500k";
            };
            kbLayout = {
              icon = "󰌌";
              label = true;
              labelType = "code";
              leftClick = "";
              middleClick = "";
              rightClick = "";
              scrollDown = "";
              scrollUp = "";
            };
            microphone = {
              label = true;
              leftClick = "menu:audio";
              middleClick = "wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle";
              mutedIcon = "󰍭";
              rightClick = "";
              scrollDown = "";
              scrollUp = "";
              unmutedIcon = "󰍬";
            };
            netstat = {
              dynamicIcon = false;
              icon = "󰖟";
              label = true;
              labelType = "full";
              leftClick = "";
              middleClick = "";
              networkInLabel = "↓";
              networkInterface = "";
              networkOutLabel = "↑";
              pollingInterval = 2000;
              rateUnit = "MiB";
              rightClick = "";
              round = true;
            };
            power = {
              icon = "";
              leftClick = "menu:powerdropdown";
              middleClick = "";
              rightClick = "";
              scrollDown = "";
              scrollUp = "";
              showLabel = true;
            };
            ram = {
              icon = "";
              label = true;
              labelType = "percentage";
              leftClick = "";
              middleClick = "";
              pollingInterval = 2000;
              rightClick = "";
              round = true;
            };
            scrollSpeed = 5;
            storage = {
              icon = "󰋊";
              label = true;
              labelType = "percentage";
              leftClick = "";
              middleClick = "";
              pollingInterval = 2000;
              rightClick = "";
              round = false;
            };
            submap = {
              disabledIcon = "󰌌";
              disabledText = "Submap off";
              enabledIcon = "󰌐";
              enabledText = "Submap On";
              label = true;
              leftClick = "";
              middleClick = "";
              rightClick = "";
              scrollDown = "";
              scrollUp = "";
              showSubmapName = true;
            };
            updates = {
              autoHide = false;
              icon = {
                pending = "󰏗";
                updated = "󰏖";
              };
              label = true;
              leftClick = "";
              middleClick = "";
              padZero = true;
              pollingInterval = 1440000;
              rightClick = "";
              scrollDown = "";
              scrollUp = "";
              updateCommand = "";
            };
            weather = {
              label = true;
              leftClick = "";
              middleClick = "";
              rightClick = "";
              scrollDown = "";
              scrollUp = "";
              unit = "metric";
            };
          };
          launcher = {
            autoDetectIcon = false;
            icon = "󱩡";
            middleClick = "hyprpanel toggleWindow settings-dialog";
            rightClick = "";
            scrollDown = "";
            scrollUp = "";
          };
          media = {
            format = " {artist: - }{title}";
            middleClick = "";
            rightClick = "";
            scrollDown = "";
            scrollUp = "";
            show_active_only = false;
            show_label = true;
            truncation = true;
            truncation_size = 30;
          };
          network = {
            label = true;
            middleClick = "";
            rightClick = "";
            scrollDown = "";
            scrollUp = "";
            showWifiInfo = true;
            truncation = true;
            truncation_size = 9;
          };
          notifications = {
            hideCountWhenZero = true;
            middleClick = "";
            rightClick = "";
            scrollDown = "";
            scrollUp = "";
            show_total = true;
          };
          volume = {
            label = true;
            middleClick = "wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle";
            rightClick = "";
            scrollDown = "${pkgs.hyprpanel}/bin/hyprpanel vol -2";
            scrollUp = "${pkgs.hyprpanel}/bin/hyprpanel vol +2";
          };
          windowtitle = {
            class_name = true;
            custom_title = true;
            icon = true;
            label = true;
            leftClick = "";
            middleClick = "";
            rightClick = "";
            scrollDown = "";
            scrollUp = "";
            truncation = false;
            truncation_size = 50;

            # TODO Options that require '{}' or '[]' are not yet implemented,
            #title_map = [
            #[
            #                "kitty"
            #                "󰄛"
            #                "Kitty Terminal"
            #]
            #[
            #                "firefox"
            #                "󰈹"
            #                "Firefox"
            #]
            #[
            #                "codium"
            #                "󰇩"
            #                "VS Code"
            #]
            #[
            #                "webcord"
            #                ""
            #                "Discord"
            #]
            #[
            #                "org.kde.dolphin"
            #                ""
            #                "Dolphin"
            #]
            #];
          };
          workspaces = {
            applicationIconEmptyWorkspace = "";
            applicationIconFallback = "󰣆";
            applicationIconOncePerWorkspace = true;
            icons = {
              active = "";
              available = "";
              occupied = "";
            };
            ignored = "-\\\\d\\\\d";
            monitorSpecific = true;
            numbered_active_indicator = "underline";
            reverse_scroll = false;
            scroll_speed = 5;
            showAllActive = true;
            showApplicationIcons = true;
            showWsIcons = true;
            show_icons = false;
            show_numbered = false;
            spacing = 1.0;
            workspaceMask = false;
            workspaces = 5;
            # TODO Options that require '{}' or '[]' are not yet implemented,
            #pplicationIconMap = {
            # "1" = "󰄛";
            # "2" = "";
            # "3" = "󰙯";
            # "4" = "󰓇";
            # "5" = "";
            #;
          };
          # FIXME
          #systray = {
          #ignore = [
          #"spotify-client"
          #];
          #};
        };

        wallpaper = {
          enable = true;
          image = "";
          pywal = false;
        };
      };
    };
  };
}
