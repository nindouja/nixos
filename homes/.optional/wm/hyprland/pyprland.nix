{
  lib,
  config,
  pkgs-unstable,
  ...
}: let
  user_monitors_ids = lib.strings.concatMapStrings (m: ''"${m.id}",'') config.nindouja.devices.monitors;
in {
  home.packages = with pkgs-unstable; [
    pyprland
  ];

  # !!! VMS and bluetooth apps opens popup on first launch. So its required to comment them as scratchapd on new install
  home.file.".config/hypr/pyprland.toml".text =
    ''
      [pyprland]
      plugins = ["expose",  "scratchpads"]

      #TODO [waybar]
      #TODO command = "waybar"
      #TODO monitors = [${user_monitors_ids}]

      # TODO not working
      [expose]
      include_special = true

      [scratchpads.default]
      hysteresis= 0.2
      hide_delay=1
      margin = 100
      unfocus = "hide"
      lazy = false
    ''
    + (
      if config.nindouja.scratchpads.dropterm
      then ''
        [scratchpads.dropterm]
        animation = "fromTop"
        command = "kitty --class dropterm"
        class = "dropterm"
        size = "80% 80%"
        use = "default"
      ''
      else ""
    )
    + (
      if config.nindouja.scratchpads.ninjaterm
      then ''

        [scratchpads.ninjaterm]
        animation = "fromLeft"
        command = "kitty --class ninjaterm --hold -- ninja help"
        class = "ninjaterm"
        size = "80% 80%"
        use = "default"
      ''
      else ""
    )
    + (
      if config.nindouja.scratchpads.music
      then ''

        [scratchpads.music]
        animation = "fromTop"
        command = "kitty --class Spotify spotify_player"
        class = "Spotify"
        size = "80% 80%"
        use = "default"
      ''
      else ""
    )
    + (
      if config.nindouja.scratchpads.bluetooth
      then ''

        [scratchpads.bluetooth]
        animation = "fromRight"
        command = "blueman-manager"
        class = ".blueman-manager-wrapped"
        size = "40% 80%"
        use = "default"
      ''
      else ""
    )
    + (
      if config.nindouja.scratchpads.volume
      then ''

        [scratchpads.volume]
        animation = "fromRight"
        command = "pavucontrol"
        class = "org.pulseaudio.pavucontrol"
        size = "40% 80%"
        use = "default"
      ''
      else ""
    )
    + (
      if config.nindouja.scratchpads.monitor
      then ''

        [scratchpads.monitor]
        animation = "fromLeft"
        command = "kitty --class monitor btm"
        class = "monitor"
        size = "80% 80%"
        use = "default"
      ''
      else ""
    )
    + (
      if config.nindouja.scratchpads.chatbot
      then ''

        [scratchpads.chatbot]
        animation = "fromLeft"
        command = "alpaca"
        class = "com.jeffser.Alpaca"
        size = "80% 80%"
        use = "default"
      ''
      else ""
    );
}
