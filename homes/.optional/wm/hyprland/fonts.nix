{
  pkgs,
  config,
  pkgs-unstable,
  ...
}: let
  theme_variant =
    if config.nindouja.appearance.darkmode
    then config.nindouja.appearance.theme.dark
    else config.nindouja.appearance.theme.light;
in {
  home.pointerCursor = {
    gtk.enable = true;
    package = theme_variant.cursors.package;
    name = theme_variant.cursors.name;
    size = 16;
  };

  gtk = {
    enable = true;
    theme = {
      package = theme_variant.cursors.package;
      name = theme_variant.cursors.name;
    };

    iconTheme = {
      package = pkgs.adwaita-icon-theme; # FIXME modularize
      name = "Adwaita";
    };

    font = {
      name = "Sans";
      size = 11;
    };

    cursorTheme = {
      package = theme_variant.cursors.package;
      name = theme_variant.cursors.name;
      size = 36;
    };
  };
}
