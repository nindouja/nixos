{pkgs-unstable, ...}: {
  home.packages = with pkgs-unstable; [
    cliphist
    wtype # To paste imediately
    wl-clipboard
    wl-clip-persist

    (pkgs.writeShellScriptBin "clipboard_manager" ''
      pkill wofi || cliphist list | wofi -dmenu | cliphist decode | wl-copy && wtype -M ctrl v -m ctrl
    '')
  ];
}
