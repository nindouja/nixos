{
  config,
  pkgs,
  ...
}: {
  home.packages = with pkgs; [
    wofi

    (pkgs.writeShellScriptBin "app_launcher" ''
      pkill wofi || wofi --show drun
    '')
  ];

  programs.wofi = {
    enable = true;
    settings = {
      show = "drun";
      width = "40%";
      height = "60%";
      prompt = "Search...";
      normal_window = true;
      location = "center";
      gtk-dark = true;
      allow_images = true;
      image_size = "48";
      insensitive = true;
      allow_markup = true;
      no_actions = true;
      orientation = "vertical";
      halign = "fill";
      content_halign = "fill";
    };
    style = ''
      window {
        /*border: 2px solid  #${config.lib.stylix.colors.base01};*/
        background-color: #${config.lib.stylix.colors.base00};
        padding: 50px;
      }

      #input {
        padding: 10px 20px;
        background-color: rgba(255, 255, 255, 0.1);
        color: #${config.lib.stylix.colors.base05};
        border-radius: 20px;
        margin: 20px 15px;
      }

      #input:focus,
      #input:selected,
      #entry:selected,
      #entry:focus {
        border-color: transparent;
      }

      #inner-box {
        margin: 20px;
      }

      #img {
        margin: 10px 10px;
      }

      #entry {
        border-radius: 20px;
        color: #${config.lib.stylix.colors.base05};
      }

      #text:selected,
      #img:selected {
        background-color: transparent;
      }

      #entry:selected {
        color: #${config.lib.stylix.colors.base07};
        background-color: #${config.lib.stylix.colors.base01};
      }
    '';
  };
}
