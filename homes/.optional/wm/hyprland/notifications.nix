{
  lib,
  config,
  pkgs-unstable,
  ...
}: let
  inherit (lib) all;
  monitors = config.nindouja.devices.monitors;
  allBarsEmpty = all (monitor: (monitor.bars or []) == []) monitors;
in {
  config = lib.mkIf (!allBarsEmpty) {
    # Hyprpanel conflicts with dunst.
    home.packages = with pkgs-unstable; [
      libnotify
      dunst
    ];

    # REF: https://github.com/dunst-project/dunst/blob/master/docs/dunst.5.pod
    home.file.".config/dunst/dunstrc".text = ''
      [global]
          monitor = 0
          follow = mouse

          sort = yes
          idle_threshold = 30
          indicate_hidden = yes
          notification_limit = 0

          progress_bar = true
          progress_bar_height = 10
          progress_bar_frame_width = 1
          progress_bar_min_width = 150
          progress_bar_max_width = 300

          # Box
          width = (300, 700)
          height = (0, 500)
          origin = top-right
          offset = (20, 20)
          scale = 0
          transparency = 100
          separator_height = 4
          padding = 4
          horizontal_padding = 8
          text_icon_padding = 0
          frame_width = 4
          frame_color = "#${config.lib.stylix.colors.base0D}"
          separator_color = "#${config.lib.stylix.colors.base05}"
          corner_radius = 10


          # Text
          font = ${config.nindouja.appearance.theme.font.name}
          markup = full
          format = "<span foreground="gray"><small>%a</small></span>\n<b>%s</b>\n%b\n"
          alignment = left
          vertical_alignment = center
          show_age_threshold = 0
          ellipsize = middle
          ignore_newline = no
          stack_duplicates = false
          hide_duplicate_count = false
          show_indicators = yes

          # Icons
          icon_position = left
          min_icon_size = 60
          max_icon_size = 60
          icon_path = /usr/share/icons/gnome/16x16/status/:/usr/share/icons/gnome/16x16/devices/

          # History
          sticky_history = yes
          history_length = 50


          ### Misc/Advanced ###
          dmenu = /usr/bin/dmenu -p dunst:
          browser = ${config.nindouja.apps.editor}
          always_run_script = true
          title = Dunst
          class = Dunst
          ignore_dbusclose = false

          # Wayland
          layer = overlay
          force_xwayland = false

          # Mouse
          mouse_left_click = do_action, open_url
          mouse_middle_click = close_current
          mouse_right_click = context


      [urgency_low]
          background = "#${config.lib.stylix.colors.base00}"
          foreground = "#${config.lib.stylix.colors.base0D}"
          frame_color = "#${config.lib.stylix.colors.base0D}"
          timeout = 3

      [urgency_normal]
          background = "#${config.lib.stylix.colors.base00}"
          foreground = "#${config.lib.stylix.colors.base0D}"
          frame_color = "#${config.lib.stylix.colors.base0A}"
          timeout = 10

      [urgency_critical]
          background = "#${config.lib.stylix.colors.base00}"
          foreground = "#${config.lib.stylix.colors.base0D}"
          frame_color = "#${config.lib.stylix.colors.base08}"
          timeout = 0


      [fullscreen_show_critical]
          msg_urgency = critical
          fullscreen = show

      [spotify]
          appname = spotify_player
          frame_color = "#1DB954"
          urgency = low
    '';
  };
}
