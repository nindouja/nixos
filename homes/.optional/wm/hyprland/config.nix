{
  pkgs,
  config,
  pkgs-unstable,
  ...
}: let
  optional_scratchpad = name: (
    if config.nindouja.scratchpads.${name}
    then "class:(${name}),"
    else ""
  );
  user_monitors =
    builtins.map (m: "${m.id}, ${m.width}x${m.height}@${m.fps}, ${m.posX}x${m.posY}, ${m.scale}, transform, ${m.transform}") config.nindouja.devices.monitors;
  user_workspaces = builtins.concatLists (builtins.map (m: builtins.map (ws: "${builtins.toString ws}, monitor:${m.id}") m.workspaces) config.nindouja.devices.monitors);
in {
  wayland.windowManager.hyprland = {
    enable = true;

    xwayland = {enable = true;};
    systemd.enable = true;

    settings = {
      "$mainMod" = "SUPER";
      "$scratchpads" =
        (optional_scratchpad "ninjaterm")
        + (optional_scratchpad "music")
        + (optional_scratchpad "bluetooth")
        + (optional_scratchpad "volume")
        + (optional_scratchpad "chatbot")
        + (optional_scratchpad "monitor");

      workspace = user_workspaces;
      monitor =
        user_monitors
        ++ [
          "Unknown-1,disable" # know bug: https://github.com/hyprwm/Hyprland/issues/5958
          ",highrr,auto,auto" # Any extra monitor
        ];

      exec-once =
        [
          "swww-daemon" # Handles backgrounds
          "pypr" # Hyprland extra features
          "hypridle" # Handles lock on inacticity timeout
          "refresh_bg" # Background

          "systemctl --user start hyprpolkitagent" # Handles pivilege elevation
          "blueman-applet" # Handles bluetooth connection
          "nm-applet --indicator" # Handles network connections

          # Start clipboard manager
          "wl-paste --type text --watch cliphist store"
          "wl-paste --type image --watch cliphist store"
        ]
        ++ ( # Starts my default apps
          if (builtins.pathExists ../../../${config.nindouja.user.username}/startup.sh)
          then ["startup"]
          else []
        );

      env = [
        "ELECTRON_OZONE_PLATFORM_HINT,auto"
      ];

      gestures = {
        workspace_swipe = true;
        workspace_swipe_fingers = 4;
      };

      misc = {
        vfr = true; # misc:no_vfr -> misc:vfr. bool, heavily recommended to leave at default on. Saves on CPU usage.
        vrr = 0; # misc:vrr -> Adaptive sync of your monitor. 0 (off), 1 (on), 2 (fullscreen only). Default 0 to avoid white flashes on select hardware.
        disable_hyprland_logo = true;
        disable_splash_rendering = true;
        animate_manual_resizes = true;
        mouse_move_enables_dpms = true; # TODO: test true = mouse dont wake up screen
      };

      input = {
        kb_layout = config.nindouja.l10n.keyboardLayout;
        numlock_by_default = 1;
        repeat_delay = 300;
        touchpad = {
          natural_scroll = true;
          clickfinger_behavior = true;
        };
      };

      cursor = {
        enable_hyprcursor = true;
      };

      xwayland = {
        # TODO force_zero_scaling = "true";
      };
    };
  };
}
