{...}: {
  imports = [
    ./bindings.nix
    ./config.nix
    ./clipboard.nix
    ./notifications.nix
    ./sleep.nix
    ./lockscreen.nix
    ./hyprpanel.nix
    ./fonts.nix
    ./main.nix
    ./pyprland.nix
    ./launcher.nix
    ./screenshot.nix
    ./waybar.nix
  ];
}
