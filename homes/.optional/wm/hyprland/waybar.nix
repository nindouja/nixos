{
  lib,
  config,
  options,
  pkgs,
  pkgs-unstable,
  ...
}: let
  inherit (lib) all;
  monitors = config.nindouja.devices.monitors;
  allBarsEmpty = all (monitor: (monitor.waybars or []) == []) monitors;
in {
  config = lib.mkIf (!allBarsEmpty) {
    home.packages = with pkgs-unstable; [
      (pkgs.writeShellScriptBin "refresh_bar" ''
        # Restart waybar
        pkill waybar
        sleep 0.5
        waybar &
      '')
    ];

    stylix.targets.waybar.enable = false;
    programs.waybar = {
      enable = true;
      package = pkgs-unstable.waybar;
      settings = lib.concatLists (builtins.map (monitor: monitor.waybars) config.nindouja.devices.monitors); # Merge all monitor's bars
      style = import ../../../../ui/waybar/style/${config.nindouja.appearance.waybar.theme} {
        inherit config;
      };
    };
  };
}
