{
  config,
  pkgs-unstable,
  ...
}: {
  home.packages = with pkgs-unstable; [
    hyprlock
  ];

  home.file.".config/hypr/hyprlock.conf".text = ''
      ignore_empty_input = true

      background {
        monitor =
        # TODO  migrate to new theme path = ${config.nindouja.appearance.theme.bg.path}
        color = rgb(${config.lib.stylix.colors.base00})

        # all these options are taken from hyprland, see https://wiki.hyprland.org/Configuring/Variables/#blur for explanations
        blur_size = 4
        blur_passes = 3 # 0 disables blurring
        noise = 0.0117
        contrast = 1.3000 # Vibrant!!!
        brightness = 0.8000
        vibrancy = 0.2100
        vibrancy_darkness = 0.0
    }

    # Clock
    label {
        monitor =
        text = cmd[update:1000] echo "<b><big> $(date +"%H:%M:%S") </big></b>"
        color = rgb(${config.lib.stylix.colors.base0C})
        font_size = 64
        font_family = ${config.nindouja.appearance.theme.font.name}
        position = 0, -200
        halign = center
        valign = top
    }

    # Now playing
    label {
        monitor =
        text = cmd[update:1000] echo "󰎈 $(playerctl -a metadata --format '{{artist}} - {{title}}' | head -1)"
        trim = text_trim
        color = rgb(${config.lib.stylix.colors.base05})
        font_size = 20
        font_family = ${config.nindouja.appearance.theme.font.name}
        position = 0, -300
        halign = center
        valign = top
    }


    # User label
    label {
        monitor =
        text = Hey <span text_transform="capitalize">$USER</span>
        color = rgb(${config.lib.stylix.colors.base0C})
        font_size = 20
        font_family = ${config.nindouja.appearance.theme.font.name}
        position = 0, -75
        halign = center
        valign = center
    }

    # Password input
    input-field {
        monitor =
        size = 250, 50
        outline_thickness = 3
        dots_size = 0.2 # Scale of input-field height, 0.2 - 0.8
        dots_spacing = 0.64 # Scale of dots' absolute size, 0.0 - 1.0
        dots_center = true
        font_color = rgb(${config.lib.stylix.colors.base0C})
        outer_color = rgb(${config.lib.stylix.colors.base05})
        inner_color = rgb(${config.lib.stylix.colors.base00})
        check_color = tgb${config.lib.stylix.colors.base07}
        fail_color = tgb${config.lib.stylix.colors.base08}
        fade_on_empty = true
        placeholder_text = <i>Password...</i> # Text rendered in the input box when it's empty.
        hide_input = false
        position = 0, -125
        halign = center
        valign = center
    }


    # Intruder alert
    label {
        monitor =
        text = Press WIN + SUPPR to enter pin
        color = rgb(${config.lib.stylix.colors.base05})
        font_size = 16
        font_family = ${config.nindouja.appearance.theme.font.name}
        position = 0, 30
        halign = center
        valign = bottom
    }

  '';
}
