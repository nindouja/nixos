{
  lib,
  pkgs-unstable,
  config,
  ...
}: let
  username = config.nindouja.user.username;
  sop_key_radarr = "vizbab/api_keys/radarr";
  sop_key_radafr = "vizbab/api_keys/radafr";
  sop_key_sonarr = "vizbab/api_keys/sonarr";
  sop_key_sonafr = "vizbab/api_keys/sonafr";
  sop_key_lidarr = "vizbab/api_keys/lidarr";
  sop_key_bazarr = "vizbab/api_keys/bazarr";
  sop_key_readarr = "vizbab/api_keys/readarr";
  sop_key_prowlarr = "vizbab/api_keys/prowlarr";
  sop_key_tautulli = "vizbab/api_keys/tautulli";
in {
  config = lib.mkIf (config.nindouja.apps.managarr) {
    home.packages = with pkgs-unstable; [
      managarr # *arr manager
    ];

    home.file.".config/managarr/config.yml".text = ''
      radarr:
        - name: "🎥 VO"
          host: 192.168.0.101
          port: 7878
          api_token_file: ${config.sops.secrets."${sop_key_radarr}".path}
        - name: "🎥 FR"
          host: 192.168.0.101
          port: 7979
          api_token_file: ${config.sops.secrets."${sop_key_radafr}".path}
      sonarr:
        - name: "🍿 VO"
          host: 192.168.0.101
          port: 8989
          api_token_file: ${config.sops.secrets."${sop_key_sonarr}".path}
        - name: "🍿 FR"
          host: 192.168.0.101
          port: 8787
          api_token_file: ${config.sops.secrets."${sop_key_sonafr}".path}
      lidarr:
        - name: "🔊"
          host: 192.168.0.101
          port: 8686
          api_token_file: ${config.sops.secrets."${sop_key_lidarr}".path}
      bazarr:
        - name: "💬"
          host: 192.168.0.101
          port: 6767
          api_token_file: ${config.sops.secrets."${sop_key_bazarr}".path}
      readarr:
        - name: "📚"
          host: 192.168.0.101
          port: 7676
          api_token_file: ${config.sops.secrets."${sop_key_readarr}".path}
      prowlarr:
        - name: "🔗"
          host: 192.168.0.101
          port: 9696
          api_token_file: ${config.sops.secrets."${sop_key_prowlarr}".path}
      tautulli:
        - name: "📊"
          host: 192.168.0.101
          port: 8181
          api_token_file: ${config.sops.secrets."${sop_key_prowlarr}".path}
    '';

    sops = {
      secrets = {
        "${sop_key_radarr}" = {};
        "${sop_key_radafr}" = {};
        "${sop_key_sonarr}" = {};
        "${sop_key_sonafr}" = {};
        "${sop_key_lidarr}" = {};
        "${sop_key_bazarr}" = {};
        "${sop_key_readarr}" = {};
        "${sop_key_prowlarr}" = {};
        "${sop_key_tautulli}" = {};
      };
    };
  };
}
