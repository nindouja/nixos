{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.geogebra) {
    home.packages = with pkgs; [
      geogebra
    ];
  };
}
