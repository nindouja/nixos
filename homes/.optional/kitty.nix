{
  lib,
  config,
  ...
}: {
  config = lib.mkIf config.nindouja.apps.kitty {
    programs.kitty = {
      enable = true;
      settings = {
        background_opacity = lib.mkForce "0.5";
        enable_audio_bell = true;
        visual_bell_color = "#${config.lib.stylix.colors.base05}";
        visual_bell_duration = "0.1";
        bell_on_tab = "󰂚 ";
        confirm_os_window_close = -0;
        copy_on_select = true;
        clipboard_control = "write-clipboard read-clipboard write-primary read-primary";
      };
    };
  };
}
