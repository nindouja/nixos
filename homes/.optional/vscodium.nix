{
  lib,
  config,
  inputs,
  ninja-base-config,
  pkgs,
  pkgs-unstable,
  ...
}: let
  extensions = inputs.nix-vscode-extensions.extensions.${ninja-base-config.system};
in {
  config = lib.mkIf config.nindouja.apps.vscodium {
    programs.vscode = {
      enable = true;
      package = pkgs-unstable.vscodium;
      extensions = with pkgs-unstable.vscode-extensions; [
        extensions.vscode-marketplace.bbenoist.nix
        extensions.vscode-marketplace.gruntfuggly.todo-tree
        extensions.vscode-marketplace.foxundermoon.shell-format
        extensions.vscode-marketplace.emeraldwalk.runonsave
        extensions.vscode-marketplace.ms-kubernetes-tools.vscode-kubernetes-tools
        extensions.vscode-marketplace.rust-lang.rust-analyzer
        extensions.vscode-marketplace.file-icons.file-icons
        extensions.vscode-marketplace.ms-vscode-remote.remote-ssh
        extensions.vscode-marketplace.ms-vscode-remote.remote-containers

        extensions.vscode-marketplace.github.copilot
        extensions.vscode-marketplace.github.copilot-chat
        extensions.vscode-marketplace.oderwat.indent-rainbow
        extensions.vscode-marketplace.mechatroner.rainbow-csv
        extensions.vscode-marketplace.tal7aouy.rainbow-bracket
        extensions.vscode-marketplace.jscearcy.rust-doc-viewer
        extensions.vscode-marketplace.taiyuuki.vscode-cargo-scripts
        extensions.vscode-marketplace.panicbit.cargo
        extensions.vscode-marketplace.brettm12345.nixfmt-vscode
      ];

      userSettings = {
        "window.titleBarStyle" = "custom";
        "keyboard.dispatch" = "keyCode";
        "git.autofetch" = true;
        "explorer.excludeGitIgnore" = true;
        "window.zoomLevel" = 1;
        "editor.formatOnSave" = true;
        "explorer.confirmDelete" = false;
        "files.autoSave" = "onFocusChange";
        "git.enableSmartCommit" = true;
        "git.confirmSync" = false;
        "editor.fontFamily" = "${config.nindouja.appearance.theme.font.name}, 'FiraCode Nerd Font'";
        "explorer.confirmDragAndDrop" = false;
      };
    };
  };

  # home.file.".config/codium-flags.conf".text = ''
  #   --enable-features=UseOzonePlatform
  #   --ozone-platform-hint=auto
  # '';
}
