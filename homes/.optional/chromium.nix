{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.chromium) {
    home.packages = with pkgs; [
      ungoogled-chromium
    ];

    home.sessionVariables = {
      CHROME = "${pkgs.ungoogled-chromium}/bin/chromium";
    };
  };
}
