{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.obsidian) {
    home.packages = with pkgs; [
      obsidian
    ];
  };
}
