{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.openscad) {
    home.packages = with pkgs; [
      openscad
    ];
  };
}
