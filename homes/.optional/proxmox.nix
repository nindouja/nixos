{
  lib,
  pkgs,
  config,
  ...
}: let
  username = config.nindouja.user.username;
  dns = config.nindouja.features.backups.dns;
  user = config.nindouja.features.backups.user;
  store = config.nindouja.features.backups.datastore;
in {
  config = lib.mkIf config.nindouja.features.backups.enable {
    home.packages = with pkgs; [
      proxmox-backup-client
      (pkgs.writeShellScriptBin "backup" ''
        proxmox-backup-client backup \
          documents.pxar:/home/${username}/Documents \
          media.pxar:/home/${username}/Media \
      '')
    ];

    home.sessionVariables = {
      # TODO: vars not set
      PBS_PASSWORD_FILE = config.sops.secrets."users/${username}/proxmox".path;
      PBS_REPOSITORY = "${user}@pbs@${dns}:${store}";
    };

    sops = {
      secrets = {
        "users/${username}/proxmox" = {};
      };
    };
  };
}
