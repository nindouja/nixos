{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.vlc) {
    home.packages = with pkgs; [
      vlc
    ];
  };
}
