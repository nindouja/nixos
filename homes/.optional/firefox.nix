{
  lib,
  pkgs,
  config,
  inputs,
  ...
}:
with builtins; let
  extension = shortId: uuid: {
    name = uuid;
    value = {
      install_url = "https://addons.mozilla.org/en-US/firefox/downloads/latest/${shortId}/latest.xpi";
      installation_mode = "force_installed";
    };
  };
  lock-false = {
    Value = false;
    Status = "locked";
  };
  lock-true = {
    Value = true;
    Status = "locked";
  };
in {
  # DOC / HELP: https://discourse.nixos.org/t/declare-firefox-extensions-and-settings/36265
  config = lib.mkIf config.nindouja.apps.firefox {
    programs = {
      firefox = {
        enable = true;
        package = pkgs.wrapFirefox pkgs.firefox-unwrapped {
          # Check about:policies#documentation for options.
          extraPolicies = {
            DisableTelemetry = true;
            DisableFirefoxStudies = true;
            EnableTrackingProtection = {
              Value = true;
              Locked = true;
              Cryptomining = true;
              Fingerprinting = true;
            };
            DisablePocket = true;
            DisableFirefoxAccounts = false;
            DisableAccounts = false;
            OverrideFirstRunPage = "";
            OverridePostUpdatePage = "";
            DisplayBookmarksToolbar = "always"; # alternatives: "always" or "newtab"
            DisplayMenuBar = "never"; # alternatives: "always", "never" or "default-on"
            SearchBar = "unified"; # alternative: "separate"

            /*
            ---- EXTENSIONS ----
            */
            ExtensionSettings =
              {
                "*".installation_mode = "blocked"; # blocks all addons except the ones specified below
              }
              // listToAttrs [
                # Must have
                (extension "proton-pass" "78272b6fa58f4a1abaac99321d503a20@proton.me")
                # (extension "bitwarden-password-manager" "{446900e4-71c2-419f-a6a7-df9c091e268b}")

                # No ads
                (extension "ublock-origin" "uBlock0@raymondhill.net")
                (extension "sponsorblock" "sponsorBlocker@ajay.app")
                (extension "youtube-ad-auto-skipper" "{bd6b8f4a-b0c3-4d61-a0f8-5539d3df3959}")

                # Misc
                (extension "custom-new-tabs" "CNTwe@ednovak.net")
                (extension "languagetool" "languagetool-webextension@languagetool.org")
                (extension "tubearchivist-companion" "{08f0f80f-2b26-4809-9267-287a5bdda2da}")
                (extension "sonarr-radarr-lidarr-search" "sonarr-radarr-lidarr-autosearch@robgreen.me")
              ];

            /*
            ---- PREFERENCES ----
            */
            # Set preferences shared by all profiles.
            Preferences = {
              "browser.contentblocking.category" = {
                Value = "strict";
                Status = "locked";
              };
              "extensions.pocket.enabled" = lock-false;
              "extensions.screenshots.disabled" = false;
              "browser.newtabpage.activity-stream.feeds.section.topstories" = false;
              "browser.newtabpage.activity-stream.feeds.snippets" = false;
              "browser.newtabpage.activity-stream.section.highlights.includePocket" = false;
              "browser.newtabpage.activity-stream.section.highlights.includeBookmarks" = false;
              "browser.newtabpage.activity-stream.section.highlights.includeDownloads" = false;
              "browser.newtabpage.activity-stream.section.highlights.includeVisited" = false;
              "browser.newtabpage.activity-stream.showSponsored" = false;
              "browser.newtabpage.activity-stream.system.showSponsored" = false;
              "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;
            };
          };
        };

        /*
        ---- PROFILES ----
        */
        # Switch profiles via about:profiles page.
        # For options that are available in Home-Manager see
        # https://nix-community.github.io/home-manager/options.html#opt-programs.firefox.profiles
        profiles = {
          profile_0 = {
            # choose a profile name; directory is /home/<user>/.mozilla/firefox/profile_0
            id = 0; # 0 is the default profile; see also option "isDefault"
            name = "Main"; # name as listed in about:profiles
            isDefault = true; # can be omitted; true if profile ID is 0
            settings = {
              # specify profile-specific preferences here; check about:config for options
              "browser.newtabpage.activity-stream.feeds.section.highlights" = false;
              "browser.startup.homepage" = "https://yo.vba.ovh";
              "browser.newtabpage.pinned" = [
                {
                  title = "Yo !";
                  url = "https://yo.vba.ovh";
                }
              ];
              # add preferences for profile_0 here...
            };
          };
        };
      };
    };
  };
}
