{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf config.nindouja.apps.kubectl {
    home.packages = with pkgs; [
      kubectl
    ];
    programs.bash.shellAliases = {
      "k" = "kubectl";
      "kl" = "kubectl get all --all-namespaces";
      "klp" = "kubectl get pods --all-namespaces";
      "kls" = "kubectl get services --all-namespaces";
      "kinfo" = "kubectl cluster-info";
      "kcleannamespace" = "kubectl delete all --all -n $1";
      "k3s---reset---hardcore" = "sudo rm -r /etc/rancher/k3s/* && sudo rm -r /var/lib/rancher/k3s/*";
    };
  };
}
