{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.plex) {
    home.packages = with pkgs; [
      plex-media-player
    ];
  };
}
