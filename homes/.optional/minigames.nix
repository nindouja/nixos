{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.features.minigames) {
    home.packages = with pkgs; [
      space-cadet-pinball
      bastet
      ninvaders
      #TODO pacman4console
      nsnake
      #TODO greed
      nudoku
      asciiquarium
    ];
  };
}
