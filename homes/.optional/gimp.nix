{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.apps.gimp) {
    home.packages = with pkgs; [
      gimp
    ];
  };
}
