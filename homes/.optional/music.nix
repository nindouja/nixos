{
  lib,
  pkgs,
  pkgs-unstable,
  config,
  ...
}: {
  config = lib.mkIf (config.nindouja.features.music) {
    home.packages =
      (with pkgs-unstable; [
        spotify
        audacity
        spotify-player
      ])
      ++ (with pkgs-unstable; [
        fum
        termusic
      ]);

    home.file.".config/spotify-player/app.toml".text = ''
      theme = "stylix"
      client_id = "e3936495e70c4253bd3cb8c0e76667cf"
      login_redirect_uri = "http://127.0.0.1:8989/login"
      client_port = 8080
      tracks_playback_limit = 200
      playback_format = "{status} {track} • {artists}\n{album}\n{metadata}"
      notify_format = { summary = "{track} • {artists}", body = "{album}" }
      notify_timeout_in_secs = 3
      app_refresh_duration_in_ms = 32
      playback_refresh_duration_in_ms = 0
      page_size_in_rows = 20
      enable_media_control = true
      enable_streaming = "DaemonOnly"
      enable_notify = true
      enable_cover_image_cache = true
      notify_streaming_only = true
      default_device = "spotify-player"
      play_icon = "▶"
      pause_icon = "▌▌"
      liked_icon = "♥"
      cover_img_length = 18
      cover_img_width = 10
      seek_duration_secs = 10
      border_type = "Rounded"

      [device]
      name = "${config.nindouja.hostname}"
      device_type = "speaker"
      volume = 85
      bitrate = 320
      audio_cache = false
      normalization = false
      autoplay = false

      [layout]
      library = { playlist_percent = 40, album_percent = 40 }
      playback_window_position = "Bottom"
      playback_window_height = 12
    '';

    home.file.".config/spotify-player/theme.toml".text = ''
      [[themes]]
      name = "stylix"
      [themes.palette]
      background = "#${config.lib.stylix.colors.base00}"
      foreground = "#${config.lib.stylix.colors.base05}"
      black = "#${config.lib.stylix.colors.base0F}"
      red = "#${config.lib.stylix.colors.base08}"
      green = "#${config.lib.stylix.colors.base0B}"
      yellow = "#${config.lib.stylix.colors.base0A}"
      blue = "#${config.lib.stylix.colors.base0D}"
      magenta = "#${config.lib.stylix.colors.base0E}"
      cyan = "#${config.lib.stylix.colors.base0C}"
      bright_black = "#${config.lib.stylix.colors.base01}"
      bright_red = "#${config.lib.stylix.colors.base09}"
      bright_green = "#${config.lib.stylix.colors.base02}"
      bright_yellow = "#${config.lib.stylix.colors.base03}"
      bright_blue = "#${config.lib.stylix.colors.base04}"
      bright_magenta = "#${config.lib.stylix.colors.base06}"
      bright_cyan = "#${config.lib.stylix.colors.base07}"
    '';

    home.file.".config/termusic/config.toml".text = ''
      music_dir = ["${config.xdg.userDirs.music}", "/mnt/media/Music/Digital", "/mnt/private/media/Music/"]
      player_port = 50101
      player_interface = "::"
      player_loop_mode = "Playlist"
      player_volume = 70
      player_speed = 10
      player_gapless = true
      podcast_simultanious_download = 3
      podcast_max_retries = 3
      podcast_dir = "/home/nindouja/Media/Music/podcast"
      player_seek_step = "Auto"
      player_remember_last_played_position = "Auto"
      enable_exit_confirmation = true
      playlist_display_symbol = true
      playlist_select_random_track_quantity = 100
      playlist_select_random_album_quantity = 5
      theme_selected = "Ashes.light"
      kill_daemon_when_quit = true
      player_use_mpris = true
      player_use_discord = true

      [album_photo_xywh]
      x_between_1_100 = 100
      y_between_1_100 = 77
      width_between_1_100 = 20
      align = "BottomRight"

      [style_color_symbol]
      library_foreground = "Foreground"
      library_background = "Reset"
      library_border = "Blue"
      library_highlight = "LightYellow"
      library_highlight_symbol = "👾"
      playlist_foreground = "Foreground"
      playlist_background = "Reset"
      playlist_border = "Blue"
      playlist_highlight = "LightYellow"
      playlist_highlight_symbol = "🔊"
      progress_foreground = "LightBlack"
      progress_background = "Reset"
      progress_border = "Blue"
      lyric_foreground = "Foreground"
      lyric_background = "Reset"
      lyric_border = "Blue"
      currently_playing_track_symbol = "🔊"

      [keys.global_esc.code]
      type = "Esc"

      [keys.global_esc.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_quit.code]
      type = "Char"
      args = "q"

      [keys.global_quit.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_left.code]
      type = "Char"
      args = "h"

      [keys.global_left.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_down.code]
      type = "Char"
      args = "j"

      [keys.global_down.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_up.code]
      type = "Char"
      args = "k"

      [keys.global_up.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_right.code]
      type = "Char"
      args = "l"

      [keys.global_right.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_goto_top.code]
      type = "Char"
      args = "g"

      [keys.global_goto_top.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_goto_bottom.code]
      type = "Char"
      args = "G"

      [keys.global_goto_bottom.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.global_player_toggle_pause.code]
      type = "Char"
      args = " "

      [keys.global_player_toggle_pause.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_player_next.code]
      type = "Char"
      args = "n"

      [keys.global_player_next.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_player_previous.code]
      type = "Char"
      args = "N"

      [keys.global_player_previous.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.global_player_volume_plus_1.code]
      type = "Char"
      args = "+"

      [keys.global_player_volume_plus_1.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.global_player_volume_plus_2.code]
      type = "Char"
      args = "="

      [keys.global_player_volume_plus_2.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_player_volume_minus_1.code]
      type = "Char"
      args = "_"

      [keys.global_player_volume_minus_1.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.global_player_volume_minus_2.code]
      type = "Char"
      args = "-"

      [keys.global_player_volume_minus_2.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_help.code]
      type = "Char"
      args = "h"

      [keys.global_help.modifier]
      type = "KeyModifiers"
      bits = 2

      [keys.global_player_seek_forward.code]
      type = "Char"
      args = "f"

      [keys.global_player_seek_forward.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_player_seek_backward.code]
      type = "Char"
      args = "b"

      [keys.global_player_seek_backward.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_lyric_adjust_forward.code]
      type = "Char"
      args = "F"

      [keys.global_lyric_adjust_forward.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.global_lyric_adjust_backward.code]
      type = "Char"
      args = "B"

      [keys.global_lyric_adjust_backward.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.global_player_speed_up.code]
      type = "Char"
      args = "f"

      [keys.global_player_speed_up.modifier]
      type = "KeyModifiers"
      bits = 2

      [keys.global_player_speed_down.code]
      type = "Char"
      args = "b"

      [keys.global_player_speed_down.modifier]
      type = "KeyModifiers"
      bits = 2

      [keys.global_lyric_cycle.code]
      type = "Char"
      args = "T"

      [keys.global_lyric_cycle.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.global_layout_treeview.code]
      type = "Char"
      args = "1"

      [keys.global_layout_treeview.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_layout_database.code]
      type = "Char"
      args = "2"

      [keys.global_layout_database.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_player_toggle_gapless.code]
      type = "Char"
      args = "g"

      [keys.global_player_toggle_gapless.modifier]
      type = "KeyModifiers"
      bits = 2

      [keys.global_config_open.code]
      type = "Char"
      args = "C"

      [keys.global_config_open.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.global_save_playlist.code]
      type = "Char"
      args = "s"

      [keys.global_save_playlist.modifier]
      type = "KeyModifiers"
      bits = 2

      [keys.global_layout_podcast.code]
      type = "Char"
      args = "3"

      [keys.global_layout_podcast.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.global_xywh_move_left.code]
      type = "Left"

      [keys.global_xywh_move_left.modifier]
      type = "KeyModifiers"
      bits = 3

      [keys.global_xywh_move_right.code]
      type = "Right"

      [keys.global_xywh_move_right.modifier]
      type = "KeyModifiers"
      bits = 3

      [keys.global_xywh_move_up.code]
      type = "Up"

      [keys.global_xywh_move_up.modifier]
      type = "KeyModifiers"
      bits = 3

      [keys.global_xywh_move_down.code]
      type = "Down"

      [keys.global_xywh_move_down.modifier]
      type = "KeyModifiers"
      bits = 3

      [keys.global_xywh_zoom_in.code]
      type = "PageUp"

      [keys.global_xywh_zoom_in.modifier]
      type = "KeyModifiers"
      bits = 3

      [keys.global_xywh_zoom_out.code]
      type = "PageDown"

      [keys.global_xywh_zoom_out.modifier]
      type = "KeyModifiers"
      bits = 3

      [keys.global_xywh_hide.code]
      type = "End"

      [keys.global_xywh_hide.modifier]
      type = "KeyModifiers"
      bits = 3

      [keys.library_load_dir.code]
      type = "Char"
      args = "L"

      [keys.library_load_dir.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.library_delete.code]
      type = "Char"
      args = "d"

      [keys.library_delete.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.library_yank.code]
      type = "Char"
      args = "y"

      [keys.library_yank.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.library_paste.code]
      type = "Char"
      args = "p"

      [keys.library_paste.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.library_search.code]
      type = "Char"
      args = "/"

      [keys.library_search.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.library_search_youtube.code]
      type = "Char"
      args = "s"

      [keys.library_search_youtube.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.library_tag_editor_open.code]
      type = "Char"
      args = "t"

      [keys.library_tag_editor_open.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.library_switch_root.code]
      type = "Char"
      args = "o"

      [keys.library_switch_root.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.library_add_root.code]
      type = "Char"
      args = "a"

      [keys.library_add_root.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.library_remove_root.code]
      type = "Char"
      args = "A"

      [keys.library_remove_root.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.playlist_delete.code]
      type = "Char"
      args = "d"

      [keys.playlist_delete.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.playlist_delete_all.code]
      type = "Char"
      args = "D"

      [keys.playlist_delete_all.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.playlist_shuffle.code]
      type = "Char"
      args = "r"

      [keys.playlist_shuffle.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.playlist_mode_cycle.code]
      type = "Char"
      args = "m"

      [keys.playlist_mode_cycle.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.playlist_play_selected.code]
      type = "Char"
      args = "l"

      [keys.playlist_play_selected.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.playlist_search.code]
      type = "Char"
      args = "/"

      [keys.playlist_search.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.playlist_swap_down.code]
      type = "Char"
      args = "J"

      [keys.playlist_swap_down.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.playlist_swap_up.code]
      type = "Char"
      args = "K"

      [keys.playlist_swap_up.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.playlist_cmus_lqueue.code]
      type = "Char"
      args = "S"

      [keys.playlist_cmus_lqueue.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.playlist_cmus_tqueue.code]
      type = "Char"
      args = "s"

      [keys.playlist_cmus_tqueue.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.database_add_all.code]
      type = "Char"
      args = "L"

      [keys.database_add_all.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.config_save.code]
      type = "Char"
      args = "s"

      [keys.config_save.modifier]
      type = "KeyModifiers"
      bits = 2

      [keys.podcast_mark_played.code]
      type = "Char"
      args = "m"

      [keys.podcast_mark_played.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.podcast_mark_all_played.code]
      type = "Char"
      args = "M"

      [keys.podcast_mark_all_played.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.podcast_episode_download.code]
      type = "Char"
      args = "d"

      [keys.podcast_episode_download.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.podcast_episode_delete_file.code]
      type = "Char"
      args = "x"

      [keys.podcast_episode_delete_file.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.podcast_delete_feed.code]
      type = "Char"
      args = "d"

      [keys.podcast_delete_feed.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.podcast_delete_all_feeds.code]
      type = "Char"
      args = "D"

      [keys.podcast_delete_all_feeds.modifier]
      type = "KeyModifiers"
      bits = 1

      [keys.podcast_search_add_feed.code]
      type = "Char"
      args = "s"

      [keys.podcast_search_add_feed.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.podcast_refresh_feed.code]
      type = "Char"
      args = "r"

      [keys.podcast_refresh_feed.modifier]
      type = "KeyModifiers"
      bits = 0

      [keys.podcast_refresh_all_feeds.code]
      type = "Char"
      args = "R"

      [keys.podcast_refresh_all_feeds.modifier]
      type = "KeyModifiers"
      bits = 1
    '';
  };
}
