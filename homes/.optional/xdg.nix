{
  lib,
  config,
  ...
}: {
  config = lib.mkIf config.nindouja.features.xdg {
    xdg = {
      enable = true;
      userDirs = {
        enable = true;
        createDirectories = true;
        music = "${config.home.homeDirectory}/Media/Music";
        videos = "${config.home.homeDirectory}/Media/Videos";
        pictures = "${config.home.homeDirectory}/Media/Pictures";
        download = "${config.home.homeDirectory}/Downloads";
        documents = "${config.home.homeDirectory}/Documents";
        desktop = null;
        publicShare = null;
        extraConfig = {
          XDG_GAME_DIR = "${config.home.homeDirectory}/Media/Games";
          XDG_GAME_SAVE_DIR = "${config.home.homeDirectory}/Media/Games/Saves";
        };
      };
    };
  };
}
