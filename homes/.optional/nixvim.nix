{
  lib,
  pkgs,
  inputs,
  config,
  ...
}: {
  imports = [inputs.nixvim.homeManagerModules.nixvim];

  config = lib.mkIf (config.nindouja.apps.nixvim.enable && config.nindouja.apps.nixvim.home-level) {
    programs.nixvim =
      {
        enable = true;
      }
      // (import ../../resources/nixvim-common.nix {
        pkgs = pkgs;
      });
  };
}
