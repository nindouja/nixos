{
  lib,
  config,
  ...
}: let
  colorW = "#${config.lib.stylix.colors.base07}";
  colorA = "#${config.lib.stylix.colors.base08}";
  colorB = "#${config.lib.stylix.colors.base09}";
  colorC = "#${config.lib.stylix.colors.base0A}";
  colorD = "#${config.lib.stylix.colors.base0B}";
  colorE = "#${config.lib.stylix.colors.base0C}";
  colorF = "#${config.lib.stylix.colors.base0D}";

  beforeModule = "$battery";
  module1 = "[](${colorA})$username$sudo";
  module2 = "[](fg:${colorA} bg:${colorB})$hostname";
  module3 = "[](fg:${colorB} bg:${colorC})$directory";
  module4 = "[](fg:${colorC} bg:${colorD})$git_branch";
  module5 = "[](fg:${colorD} bg:${colorE})$status$cmd_duration";
  module6 = "[](fg:${colorE} bg:${colorF})$nix_shell$jobs";
  afterModule = "[](fg:${colorF}) $character";
in {
  config = lib.mkIf config.nindouja.apps.starship {
    programs.starship = {
      enable = true;
      enableBashIntegration = true;

      settings = {
        add_newline = false;
        format = "${beforeModule}${module1}${module2}${module3}${module4}${module5}${module6}${afterModule}";
        right_format = "$git_metrics$git_commit$git_state$git_status$java$gradle$kotlin$nodejs$time";
        continuation_prompt = "[󰇘](bright-black) ";

        # Left prompt
        battery = {
          format = "[$symbol$percentage]($style) ";
          charging_symbol = "⚡️ ";
          discharging_symbol = "󰂃 ";
          empty_symbol = "💀 ";
          full_symbol = "🔋 ";
          disabled = false;
          display = [
            {
              style = "red bold";
              threshold = 100;
            }
          ];
        };
        username = {
          show_always = true;
          style_user = "bg:${colorA} fg:${colorW}";
          style_root = "bg:${colorA} fg:${colorW}";
          format = "[$user ]($style)";
        };
        sudo = {
          disabled = false;
          style = "bg:${colorA} fg:${colorW}";
          format = "[as $symbol]($style)";
        };
        hostname = {
          ssh_only = false;
          style = "bg:${colorB} fg:${colorW}";
          format = "[ $ssh_symbol$hostname ]($style)";
          ssh_symbol = " ";
        };
        directory = {
          disabled = false;
          style = "bg:${colorC} fg:${colorW}";
          format = "[ $read_only$path ]($style)";
          home_symbol = "~";
          read_only = " ";
          repo_root_format = "[$before_root_path]($style)[$repo_root]($repo_root_style)[$path]($style)[$read_only]($read_only_style) ";
          truncate_to_repo = false;
          truncation_length = 30;
          truncation_symbol = "…/";
          use_logical_path = true;
          use_os_path_sep = true;
        };
        git_branch = {
          only_attached = true;
          style = "bg:${colorD} fg:${colorW}";
          format = "[ $symbol $branch ]($style)";
          symbol = "";
        };
        status = {
          style = "bg:${colorE} fg:${colorW}";
          disabled = false;
          format = "[ $symbol$status in]($style)";
        };
        cmd_duration = {
          min_time = 0;
          show_milliseconds = true;
          show_notifications = true;
          min_time_to_notify = 10000;
          style = "bg:${colorE} fg:${colorW}";
          format = "[ $duration ]($style)";
        };
        nix_shell = {
          symbol = "";
          style = "bg:${colorF} fg:${colorW}";
          format = "[ $symbol ]($style)";
        };
        jobs = {
          style = "bg:${colorF} fg:${colorW}";
          format = "[ $symbol$number ]($style)";
        };
        character = {
          success_symbol = "[](fg:${colorW})";
          error_symbol = "[](fg:${colorW})";
        };

        # Right prompt
        time = {
          disabled = false;
          format = "[\\\[ $time \\\]]($style)";
          time_format = "%T";
        };
        git_metrics = {
          disabled = false;
          format = "([+$added]($added_style) )([-$deleted]($deleted_style))  ";
        };
        git_commit = {
          only_detached = true;
          format = "[\($hash$tag\)]($style)  ";
          commit_hash_length = 6;
          tag_symbol = " 🏷 ";
        };
        git_state = {
          format = "\([$state( $progress_current/$progress_total)]($style)\)  ";
        };
        git_status = {
          ahead = "";
          behind = "";
          diverged = "󰃻";
          format = "([\\\[$all_status$ahead_behind\\\]]($style))  ";
        };
        java = {
          disabled = false;
          format = "[$symbol($version)]($style)  ";
          symbol = "";
        };
        gradle = {
          disabled = false;
          format = "[$symbol($version)]($style)  ";
          symbol = "";
        };
        kotlin = {
          disabled = false;
          format = "[$symbol($version)]($style)  ";
          symbol = "";
        };
        nodejs = {
          disabled = false;
          format = "[$symbol($version)]($style)  ";
          symbol = "󰎙";
        };
      };
    };
  };
}
