nix flake lock --update-input ninja --extra-experimental-features "nix-command flakes"
nix flake lock --update-input ninja-base-config --extra-experimental-features "nix-command flakes"
nix develop --extra-experimental-features "nix-command flakes"
