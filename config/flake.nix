{
  description = "Converts JSON into a flake";
  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-24.05";
    };
  };
  outputs = {
    self,
    nixpkgs,
  }:
    nixpkgs.lib.trivial.importJSON ./base-config.json;
}
