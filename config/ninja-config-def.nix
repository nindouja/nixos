{
  lib,
  pkgs,
  config,
  ninja-base-config,
  ...
}: let
  inherit (lib) mkOption;
  inherit (lib.types) uniq bool int str attrs attrsOf listOf submodule package path;

  T_str = mkOption {type = str;};
  T_booleanDefaultFalse = mkOption {
    type = bool;
    default = false;
  };
  T_booleanDefaultTrue = mkOption {
    type = bool;
    default = true;
  };
in {
  config.nindouja = {
    system = ninja-base-config.system;
    hostname = ninja-base-config.hostname;
    user.username = ninja-base-config.username;
    user.publicKey = builtins.readFile ../homes/.keys/id_${ninja-base-config.username}.pub;
  };

  # TODO check all default
  options.nindouja = {
    system = mkOption {type = uniq str;};
    hostname = mkOption {type = uniq str;};

    # TODO vpn
    peers = mkOption {
      type = attrsOf (submodule {
        options = {
          ip = T_str;
          key = T_str;
          endpoint = T_str;
        };
      });
    };

    user = {
      username = mkOption {type = uniq str;};
      name = T_str;
      email = T_str;
      publicKey = mkOption {type = uniq str;};
      extraSshAuthProfile = mkOption {
        type = listOf str;
        default = ["nindouja"];
      };
    };

    l10n = {
      timezone = T_str;
      languageLocale = T_str;
      formatLocale = T_str;
      keyboardLayout = T_str;
    };

    appearance = {
      theme = {
        bg = {
          path = mkOption {
            type = path;
            default = ../ui/wallpapers/tokyo-by-night.png;
          };
          url = mkOption {
            type = str;
            default = "";
          };
          sha256 = mkOption {
            type = str;
            default = "";
          };
        };

        font = {
          name = mkOption {
            type = str;
            default = "Intel One Mono";
          };
          package = mkOption {
            type = package;
            default = pkgs.intel-one-mono;
          };
        };

        dark = {
          palette = {
            name = mkOption {
              type = str;
              default = "";
            };
            yaml = mkOption {
              type = attrs;
              default = {};
            };
          };
          cursors = {
            package = mkOption {
              type = package;
              default = pkgs.material-cursors;
            };
            name = mkOption {
              type = str;
              default = "material-cursors-dark";
            };
          };
        };

        light = {
          palette = {
            name = mkOption {
              type = str;
            };
            yaml = mkOption {
              type = attrs;
            };
          };
          cursors = {
            package = mkOption {
              type = package;
              default = pkgs.material-cursors;
            };
            name = mkOption {
              type = str;
              default = "material-cursors-light";
            };
          };
        };
      };
      darkmode = T_booleanDefaultTrue;
      window-manager = mkOption {
        type = str;
        default = ninja-base-config.window-manager;
      };
      waybar = {
        # REF: ui/waybar/style
        theme = mkOption {
          type = str;
          default = "none";
        };
      };
      hyprpanel = {
        # REF: https://hyprpanel.com/configuration/themes.html
        baseTheme = mkOption {
          type = str;
          default = "default";
        };
        # REF: ui/hyprpanel/style
        customTheme = mkOption {
          type = str;
          default = "rose_pine_moon";
        };
      };
    };

    devices = {
      mouse = T_str;
      keyboard = T_str;
      swap_size = mkOption {
        type = int;
        default = 0;
      };
      eth = mkOption {
        # ls /sys/class/net/NAME
        type = str;
        default = "none";
      };
      wifi = mkOption {
        # ls /sys/class/net/NAME
        type = str;
        default = "none";
      };
      keyboardBacklight = mkOption {
        # ls /sys/class/backlight/NAME
        type = str;
        default = "none";
      };
      screenBacklight = mkOption {
        # ls /sys/class/leds/NAME
        type = str;
        default = "none";
      };
      battery = mkOption {
        # ls /sys/class/power_supply/NAME
        type = str;
        default = "none";
      };
      mainThermalZone = mkOption {
        type = int;
        default = 0;
      };
      hasSoundSupport = T_booleanDefaultTrue;
      monitors = mkOption {
        type = listOf (submodule {
          options = {
            id = T_str;
            width = T_str;
            height = T_str;
            posX = T_str;
            posY = T_str;
            scale = mkOption {
              type = str;
              default = "1";
            };
            fps = T_str;
            workspaces = mkOption {
              type = listOf int;
              default = [1 2 3 4 5 6];
            };
            # If set waybar will be installed
            # REF: ui/waybar/layout
            waybars = mkOption {
              type = listOf attrs;
              default = [];
            };
            # If set hyprpanel will be installed
            # REF: ui/hyprpanel/layout
            hyprpanel = mkOption {
              type = str;
              default = "none";
            };
            transform = mkOption {
              type = str;
              default = "0"; # https://wiki.hyprland.org/Configuring/Monitors/#rotating
            };
          };
        });
      };
    };

    bash = {
      aliases = mkOption {
        type = attrs;
        default = {};
      };
      extraBashRc = mkOption {
        type = str;
        default = "";
      };
    };

    apps = {
      editor = T_str;
      browser = T_str;
      nixvim = {
        enable = mkOption {
          type = bool;
          default = true;
        };
        home-level = mkOption {
          type = bool;
          default = false;
        };
      };
      kubectl = T_booleanDefaultFalse;
      alacritty = T_booleanDefaultFalse;
      kitty = T_booleanDefaultFalse;
      starship = T_booleanDefaultFalse;
      steam = T_booleanDefaultFalse;
      vscodium = T_booleanDefaultFalse;
      cava = T_booleanDefaultFalse;
      firefox = T_booleanDefaultFalse;
      chromium = T_booleanDefaultFalse;
      managarr = T_booleanDefaultFalse;
      discord = T_booleanDefaultFalse;
      element = T_booleanDefaultFalse;
      obsidian = T_booleanDefaultFalse;
      vlc = T_booleanDefaultFalse;
      plex = T_booleanDefaultFalse;
      geogebra = T_booleanDefaultFalse;
      gimp = T_booleanDefaultFalse;
      krita = T_booleanDefaultFalse;
      inkscape = T_booleanDefaultFalse;
      blender = T_booleanDefaultFalse;
      openscad = T_booleanDefaultFalse;
      obs = T_booleanDefaultFalse;
      alpaca = T_booleanDefaultFalse;
    };

    dev = {
      java = T_booleanDefaultFalse;
      rust = T_booleanDefaultFalse;
      go = T_booleanDefaultFalse;
    };

    features = {
      docker = T_booleanDefaultFalse;
      k3s = T_booleanDefaultFalse;
      vms = T_booleanDefaultFalse;
      openrgb = T_booleanDefaultFalse;
      ssh = T_booleanDefaultFalse;
      bluetooth = T_booleanDefaultFalse;
      nvidia-drivers = T_booleanDefaultFalse;
      input-remapper = T_booleanDefaultFalse;
      xdg = T_booleanDefaultFalse;
      ollama = T_booleanDefaultFalse;
      minigames = T_booleanDefaultFalse;
      minecraft = T_booleanDefaultFalse;
      music = T_booleanDefaultFalse;

      lan_vpn = {
        enable = T_booleanDefaultFalse;
        publicKey = T_str;
      };
      backups = {
        enable = T_booleanDefaultFalse;
        dns = T_str;
        user = mkOption {
          type = str;
          default = config.nindouja.user.username;
        };
        datastore = mkOption {
          type = str;
          default = "home-${config.nindouja.user.username}";
        };
      };
      proton-sync = {
        enable = T_booleanDefaultFalse;
        username = mkOption {
          type = str;
          default = config.nindouja.user.username;
        };
      };
    };

    scratchpads = {
      dropterm = T_booleanDefaultTrue;
      ninjaterm = T_booleanDefaultTrue;
      music = T_booleanDefaultTrue;
      bluetooth = T_booleanDefaultTrue;
      volume = T_booleanDefaultTrue;
      monitor = T_booleanDefaultTrue;
      chatbot = T_booleanDefaultTrue;
    };

    shares = {
      isos = {
        enable = T_booleanDefaultTrue;
        useTag = T_booleanDefaultFalse;
      };
      media = {
        enable = T_booleanDefaultTrue;
        useTag = T_booleanDefaultFalse;
      };
      public = {
        enable = T_booleanDefaultTrue;
        useTag = T_booleanDefaultFalse;
      };
      backup = {
        enable = T_booleanDefaultTrue;
        useTag = T_booleanDefaultFalse;
      };
      games = {
        enable = T_booleanDefaultTrue;
        useTag = T_booleanDefaultFalse;
      };
      private = {
        enable = T_booleanDefaultTrue;
        useTag = T_booleanDefaultFalse;
      };
      appdata = {
        enable = T_booleanDefaultTrue;
        useTag = T_booleanDefaultFalse;
      };
    };
  };
}
