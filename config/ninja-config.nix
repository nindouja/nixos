{
  lib,
  config,
  ninja-base-config,
  ...
}: {
  imports = [
    ./ninja-config-def.nix
    ../hosts/${ninja-base-config.hostname}/config.nix
    ../homes/${ninja-base-config.username}/config.nix
  ];
}
