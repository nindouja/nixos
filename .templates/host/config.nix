{ninja-base-config, ...}: {
  config.host = {
    hostname = ninja-base-config.hostname;
    system = ninja-base-config.system;

    l10n = {
      timezone = "Europe/Paris";
      languageLocale = "en_US.UTF-8";
      formatLocale = "fr_FR.UTF-8";
      keyboardLayout = "fr";
    };

    devices = {
      mouse = "";
      keyboard = "";
      keyboardBacklight = "";
      screenBacklight = "";
      monitors = [];
    };
  };
}
