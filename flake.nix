{
  description = "Nindouja's flake";

  nixConfig = {
    extra-substituters = [
      "https://nix-community.cachix.org"
    ];
    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9efefefefeqf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  outputs = inputs @ {
    nixpkgs,
    ninja-base-config,
    nixpkgs-unstable,
    home-manager,
    stylix,
    ninja,
    ...
  }: let
    commonArgs = {
      inherit system;
      config.allowUnfree = true;
    };
    pkgs = import nixpkgs commonArgs;
    pkgs-unstable = import nixpkgs-unstable commonArgs;
    system = ninja-base-config.system;
    lib = nixpkgs.lib;

    must-have = import ./resources/must-have.nix {inherit pkgs pkgs-unstable inputs;};
  in {
    # Generic system config (call with --flake ~/.dotfiles#system). Actual host is based on .hostname file content
    nixosConfigurations = {
      system = lib.nixosSystem {
        system = ninja-base-config.system;
        modules = [
          ./config/ninja-config.nix # Custom config

          hosts/.common # Host's common modules
          hosts/.optional # Host's optional modules activated via the config
          hosts/${ninja-base-config.hostname} # This host's special config
          hosts/${ninja-base-config.hostname}/configuration.nix
          hosts/${ninja-base-config.hostname}/hardware-configuration.nix
          hosts/.optional/wm/${ninja-base-config.window-manager} # Chosen window manager config
        ];
        specialArgs = {
          inherit inputs;
          inherit ninja-base-config;
          inherit pkgs-unstable;

          inherit stylix;
          inherit ninja;
        };
      };
    };

    # Generic home config (call with --flake ~/.dotfiles#user). Actual user is based on .username file content
    homeConfigurations = {
      user = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [
          ./config/ninja-config.nix # Custom config

          homes/.common # User's common modules
          homes/.optional # User's optional modules activated via the config
          homes/${ninja-base-config.username} # This user's special config
          homes/.optional/wm/${ninja-base-config.window-manager} # Chosen window manager configc
        ];
        extraSpecialArgs = {
          inherit inputs;
          inherit ninja-base-config;
          inherit pkgs-unstable;

          inherit stylix;
          inherit ninja;
        };
      };
    };

    devShells.${system}.default = nixpkgs.legacyPackages.${system}.mkShell {
      packages = must-have.must-have-pkgs;
      shellHook = builtins.concatStringsSep "\n" ((builtins.attrValues (builtins.mapAttrs (key: value: "alias ${key}='${value}'") must-have.must-have-aliases)) ++ [''export NIX_CONFIG="experimental-features = nix-command flakes"'']);
    };
  };

  inputs = {
    # Base OS
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-24.11";
    };
    nixpkgs-unstable = {
      url = "github:Nindouja/nixpkgs/master";
      #url = "github:NixOS/nixpkgs/master";
      # url = "github:NixOS/nixpkgs/nixos-unstable";
    };
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11/";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware = {
      url = "github:NixOS/nixos-hardware/master";
    };
    nixvim = {
      url = "github:nix-community/nixvim/nixos-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    ninja-base-config = {
      url = "path:config";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Window manager
    stylix = {
      url = "github:danth/stylix/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hyprpanel = {
      url = "github:jas-singhfsu/hyprpanel";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Secrets
    sops-nix = {
      url = "github:mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    ninja-secrets = {
      url = "git+https://gitlab.vba.ovh/Nindouja/secrets.git?ref=main&shallow=1";
      flake = false;
    };

    # Utils
    ninja.url = "git+https://gitlab.vba.ovh/Nindouja/ninja.git";
    nix-vscode-extensions.url = "github:nix-community/nix-vscode-extensions";
  };
}
