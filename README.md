# Install procedure

```
nix flake lock --update-input ninja --extra-experimental-features "nix-command flakes"
nix flake lock --update-input ninja-base-config --extra-experimental-features "nix-command flakes"
nix develop --extra-experimental-features "nix-command flakes"
```


## Why do I exist ?

My only purpose is to facilitate install and configuration process of Nindouja's different computer, servers and hopefully up to his smartphone ([maybe someday](https://github.com/mobile-nixos/mobile-nixos)).
All using only the configurations files present in this repo. As it is, it might be too much tailored to my needs to be forked by someone to use,
but I'm keeping it public so hopefully people can get insipration from it. This config would not be possible without the inspiration I got from some [other awesome configs](#sources-of-inspiration).

So much thanks to **them** and [other people that helped me](https://discourse.nixos.org/u/nindouja/summary) on the nix forums ❤️ !


## How am I designed ?

Designed is a fancy word here... 

~~For the time being I'm just ading stuff and sometime refactoring a bit. ~~ => Still partially true; but things are much more organized now.

Hoppefully things will be even much better once I finish stuff on the [TODO list](/TODO.md). It's a good resources to know things that does not work properly before you copy anything.

Anyway, the structure is heavily inspired by EmergentMind's own config structure. Host and Home are treated as 2 complete separate things. 
When instaling I install **one** home profile and **one** host profile. I have no need for multiple users one one host for now.


### Main structure

Here is a visual representation of the structure. More details below

```
.dotfiles                                   
├── .gitignore                              
├── .templates                       #
│  ├── home                          # A sample home profile
│  │  ├── config.nix                 # That home profile configuration
│  │  └── default.nix                        
│  └── host                          # A sample host profile
│     ├── config.nix                 # That host profile configuration
│     └── default.nix                       
├── config                           # My own config options to make this dotfiles modular. Refered to as ninja-config
│  ├── base-config.json              # A JSON that contains this host/home basic values needed for the build. (not tracked in git)
│  ├── flake.nix                            
│  ├── ninja-config-def.nix          # The declaration of a module describing all the config options of this dotfiles
│  └── ninja-config.nix              # Shortcut to import the base config as well as the host/home specific configs
├── flake.lock                              
├── flake.nix                               
├── homes                            # Holds any configuration related to the home profile configuration (via home-manager)
│  ├── .common                       # Configs that are required for all users
│  │  ├── ...
│  ├── .keys                         # All my users public keys. Private keys are in a secret repo.
│  │  ├── ...
│  ├── .optional                     # Configs that are activated or not via ninja-config
│  │  ├── ...
│  ├── magik                         # One of my user configuration
│  │  ├── config.nix                 # That user ninja-config overrides
│  │  └── ...                        # May also contain more files for specifi stuff
├── hosts                            # Holds any configuration related to the host profile configuration (via home-manager)
│  ├── .common                       # Configs that are required for all hostnames
│  │  ├── ...
│  ├── .optional                     # Configs that are activated or not via ninja-config-def
│  │  ├── ...
│  └── MAGIKUBE                      # One of my host configuration
│     ├── config.nix                 # That host ninja-config overrides
│     ├── configuration.nix          # That host initial configuration.nix
│     ├── hardware-configuration.nix # That host initial hardware-configuration.nix
│     └── ...                        # May also contain more files for specifi stuff

```

For a better user experience the dotfiles are meant to be used via the included nix shell so [Ninja script](https://gitlab.vba.ovh/Nindouja/ninja/) and some other dependencies are available.


### Host profiles

**Each host (aka, basicaly a motherboard with stuff pluged in it) I own got it's own profile under the [/hosts](/hosts/) folder**. 


Each one of those host profile can define a custom config file. **This is not nix configuration.nix file**, it is jut a basic nix file; it is supposed to be an "implementation" or override of [ninja-config](/config/ninja-config-def.nix).

When building the host config, the following modules are imported from the flake:
* ninja-config - includes all (base, home **and** host config)
* `/hosts/.common/` folder - all stuff common to all hosts
* `/hosts/.optional/` folder - all modular stuff depending on ninja-config
* the `/hosts/SELECTED_HOST/default.nix` - If the host has specific stuff it goes there
* the `/hosts/SELECTED_HOST/hardware-configuration.nix`  
* the `/hosts/SELECTED_HOST/configuration.nix` 
* the `/hosts/SELECTED_WINDOW_MAANGER/` folder - all (host level) stuff related to the window manager chosen


### Home profiles

**Much like hosts; each home/user I regularly use (aka, a prsonal or work profile; or just a maintenance user on a server) got it's own profile under the [/homes](/homes/) folder**. 


Also like hosts; each one of those home profile can define a custom config file overriding of [ninja-config](/config/ninja-config-def.nix).

When building the home config, the following modules are imported from the flake:
* ninja-config - includes all (base, home **and** host config)
* `/homes/.common/` folder - all stuff common to all homes
* `/homes/.optional/` folder - all modular stuff depending on ninja-config
* the `/homes/SELECTED_HOME/default.nix` - If the user has specific stuff it goes there
* the `/homes/SELECTED_WINDOW_MAANGER/` folder - all (home level) stuff related to the window manager chosen


### Templates

The [.templates](/.templates/) folder contains a temaplate for:
* one host basic config; **the default hardware-config and configuration file (found inside `/etc/nixos/`) should be added !
* one home config

Those are temapltes as the title says; those should be edited before being used !



## Resources

### Sources of inspiration

##### Youtube stuff

* [Vimjoyer](https://www.youtube.com/watch?v=bjTxiFLSNFA&list=PLko9chwSoP-15ZtZxu64k_CuTzXrFpxPE)
* [LibrePhoenix](https://www.youtube.com/playlist?list=PL_WcXIXdDWWpuypAEKzZF2b5PijTluxRG)

##### Other configs

* [LibrePhoenix](https://gitlab.com/librephoenix/nixos-config)
* [EmergentMind](https://github.com/EmergentMind/nix-config)
* [redyf](https://github.com/redyf/nixdots)



### Some usefull links

* Online and searchable [list of Nix packages](https://mynixos.com/)
* [Packages reference with details about version and their commit](https://www.nixhub.io/)
* Nix [manual](https://nixos.org/manual)
* Alternate [manual](https://ryantm.github.io/nixpkgs/) with more function reference
* All mkoptiosn [types reference](https://nixos.wiki/wiki/Declaration)






