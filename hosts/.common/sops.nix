{
  inputs,
  config,
  ...
}: let
  username = config.nindouja.user.username;
  secretsDirectory = builtins.toString inputs.ninja-secrets;
  homeDirectory = "/home/${username}";
in {
  imports = [
    inputs.sops-nix.nixosModules.sops
  ];

  sops = {
    defaultSopsFile = "${secretsDirectory}/secrets.yaml";
    validateSopsFiles = false;
    age = {
      # automatically import this user and the dev ssh keys as age keys
      sshKeyPaths = ["/etc/ssh/id_ed25519_ninja"];
      # this will use an age key that is expected to already be in the filesystem
      keyFile = "/var/lib/sops-nix/key.txt";
      # generate a new key if the key specified above does not exist
      generateKey = true;
    };
  };
}
