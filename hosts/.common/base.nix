{
  lib,
  pkgs,
  config,
  ...
}: let
  ifTheyExist = groups: builtins.filter (group: builtins.hasAttr group config.users.groups) groups;
  userPasswordSopKey = "users/${config.nindouja.user.username}/session";
  username = config.nindouja.user.username;
  homeDirectory = "/home/${username}";
in {
  nixpkgs.config.allowUnfree = true;
  nix.settings.experimental-features = ["nix-command" "flakes"];

  users = {
    mutableUsers = false; # Required for password to be set via sops during system activation!
    users.${username} = {
      isNormalUser = true;
      hashedPasswordFile = config.sops.secrets."${userPasswordSopKey}".path; # Generated with `mkpasswd MY_PASSWORD`
      description = lib.mkForce config.nindouja.user.name;
      extraGroups =
        ["wheel"]
        ++ ifTheyExist [
          "audio"
          "video"
          "docker"
          "git"
          "networkmanager"
        ];
    };
  };

  sops = {
    secrets = {
      "private_keys/${username}" = {
        owner = config.users.users.${username}.name;
        inherit (config.users.users.${username}) group;
        path = "${homeDirectory}/.ssh/id_ed25519";
      };
      "${userPasswordSopKey}" = {neededForUsers = true;};
    };
  };

  #TODO move
  environment.shells = with pkgs; [bash];
  users.defaultUserShell = pkgs.bash;

  boot.loader.grub.configurationLimit = 30;
}
