{
  lib,
  config,
  ...
}: {
  networking.hostName = lib.mkForce config.nindouja.hostname;
  networking.networkmanager.enable = true;
}
