{
  lib,
  config,
  ...
}: {
  time.timeZone = lib.mkForce config.nindouja.l10n.timezone;
  console.keyMap = lib.mkForce config.nindouja.l10n.keyboardLayout;

  services.xserver = {
    xkb = {
      layout = lib.mkForce config.nindouja.l10n.keyboardLayout;
      variant = lib.mkForce "";
    };
  };

  i18n.defaultLocale = lib.mkForce config.nindouja.l10n.languageLocale;
  i18n.extraLocaleSettings = {
    LC_ADDRESS = lib.mkForce config.nindouja.l10n.formatLocale;
    LC_IDENTIFICATION = lib.mkForce config.nindouja.l10n.formatLocale;
    LC_MEASUREMENT = lib.mkForce config.nindouja.l10n.formatLocale;
    LC_MONETARY = lib.mkForce config.nindouja.l10n.formatLocale;
    LC_NAME = lib.mkForce config.nindouja.l10n.formatLocale;
    LC_NUMERIC = lib.mkForce config.nindouja.l10n.formatLocale;
    LC_PAPER = lib.mkForce config.nindouja.l10n.formatLocale;
    LC_TELEPHONE = lib.mkForce config.nindouja.l10n.formatLocale;
    LC_TIME = lib.mkForce config.nindouja.l10n.formatLocale;
  };
}
