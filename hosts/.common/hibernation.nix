{
  pkgs,
  pkgs-unstable,
  inputs,
  ...
}: {
  systemd.sleep.extraConfig = ''
    HibernateDelaySec=30m
  '';
}
