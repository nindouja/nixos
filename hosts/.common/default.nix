{
  pkgs,
  pkgs-unstable,
  inputs,
  ...
}: {
  imports = [
    ./allow-reboots.nix
    ./base.nix
    ./hibernation.nix
    ./locale.nix
    ./network.nix
    ./security.nix
    ./sops.nix
  ];

  environment.systemPackages = (import ../../resources/must-have.nix {inherit pkgs pkgs-unstable inputs;}).must-have-pkgs;
}
