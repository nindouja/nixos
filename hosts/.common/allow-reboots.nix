{config, ...}: {
  security.sudo.extraRules = [
    {
      users = [config.nindouja.user.username];
      commands = [
        {
          command = "/run/current-system/sw/bin/shutdown";
          options = ["NOPASSWD"];
        }
      ];
    }
  ];
}
