{
  lib,
  pkgs-unstable,
  config,
  inputs,
  ...
}: {
  config = lib.mkIf config.nindouja.dev.go {
    environment.systemPackages = with pkgs-unstable; [
      (jetbrains.plugins.addPlugins jetbrains.goland (import ../../resources/idea-plugins.nix))
    ];
  };
}
