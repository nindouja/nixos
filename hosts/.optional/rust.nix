{
  lib,
  pkgs-unstable,
  config,
  inputs,
  ...
}: {
  config = lib.mkIf config.nindouja.dev.rust {
    environment.systemPackages = with pkgs-unstable; [
      teamspeak3 # TODO move
      # Basics
      lld
      cmake
      gcc
      gnumake
      # cargo ; no needed anymore
      rustc
      rustup
      rustfmt

      # Needed for some libs
      openssl
      openssl.dev
      pkg-config

      (jetbrains.plugins.addPlugins jetbrains.rust-rover (import ../../resources/idea-plugins.nix))
    ];

    environment.variables = {
      OPENSSL_LIB_DIR = "${pkgs-unstable.openssl.out}/lib";
      OPENSSL_DIR = "${pkgs-unstable.openssl.dev}";
      PKG_CONFIG_PATH = "${pkgs-unstable.openssl.dev}/lib/pkgconfig";
    };
  };
}
