{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkIf config.nindouja.features.docker {
    virtualisation.docker = {
      enable = true;
      rootless = {
        enable = true;
        setSocketVariable = true;
      };
    };

    environment.systemPackages = with pkgs; [
      ctop
    ];
  };
}
