{
  lib,
  pkgs,
  config,
  ...
}: {
  config = lib.mkMerge [
    (lib.mkIf config.nindouja.features.vms {
      programs.virt-manager.enable = true;

      environment.systemPackages = with pkgs; [
        virt-manager
        qemu_kvm
        qemu
      ];

      users.users.${config.nindouja.user.username} = {
        extraGroups = ["libvirtd"];
      };

      virtualisation.libvirtd = {
        allowedBridges = [
          "nm-bridge"
          "virbr0"
        ];
        enable = true;
        qemu = {
          package = pkgs.qemu_kvm;
          runAsRoot = true;
          swtpm.enable = true;
          ovmf = {
            enable = true;
            packages = [
              (pkgs.OVMF.override {
                secureBoot = true;
                tpmSupport = true;
              })
              .fd
            ];
          };
        };
      };
    })

    (lib.mkIf config.nindouja.features.docker {
      virtualisation.docker = {
        enable = true;
        rootless = {
          enable = true;
          setSocketVariable = true;
        };
      };
    })
  ];
}
