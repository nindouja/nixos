{
  lib,
  config,
  ...
}: let
  build = name: useTag: {
    fsType = "nfs";
    device = "192.168.0.100:/mnt/user/${name}";
    #options = ["defaults"];
  };
in {
  fileSystems."/mnt/backup" = lib.mkIf config.nindouja.shares.backup.enable (build "backup" config.nindouja.shares.backup.useTag);
  fileSystems."/mnt/games" = lib.mkIf config.nindouja.shares.games.enable (build "games" config.nindouja.shares.games.useTag);
  fileSystems."/mnt/isos" = lib.mkIf config.nindouja.shares.isos.enable (build "isos" config.nindouja.shares.isos.useTag);
  fileSystems."/mnt/media" = lib.mkIf config.nindouja.shares.media.enable (build "media" config.nindouja.shares.media.useTag);
  fileSystems."/mnt/private" = lib.mkIf config.nindouja.shares.private.enable (build "private" config.nindouja.shares.private.useTag);
  fileSystems."/mnt/public" = lib.mkIf config.nindouja.shares.public.enable (build "public" config.nindouja.shares.public.useTag);
  fileSystems."/mnt/appdata" = lib.mkIf config.nindouja.shares.appdata.enable (build "appdata" config.nindouja.shares.appdata.useTag);
}
