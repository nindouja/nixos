{
  lib,
  inputs,
  config,
  ...
}: let
  username = config.nindouja.user.username;
  secretsDirectory = builtins.toString inputs.ninja-secrets;
  homeDirectory = "/home/${username}";
in {
  config = lib.mkIf (config.nindouja.devices.swap_size != 0) {
    swapDevices = [
      {
        device = "/swapfile";
        size = config.nindouja.devices.swap_size * 1024;
      }
    ];
  };
}
