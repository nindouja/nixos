{
  lib,
  config,
  ...
}: let
  wgPrivateKeySopKey = "hosts/${config.nindouja.hostname}/vpn_keys/LAN";
in {
  config = lib.mkIf config.nindouja.features.lan_vpn.enable {
    networking.firewall = {
      allowedUDPPorts = [51820];
    };

    networking.wireguard.interfaces = {
      wg0 = {
        ips = ["10.253.0.4/24"]; # IP address of this machine in the *tunnel network*
        listenPort = 51820;
        privateKeyFile = config.sops.secrets."${wgPrivateKeySopKey}".path;

        peers = [
          {
            publicKey = config.nindouja.features.lan_vpn.publicKey; # Public key of the server (not a file path).
            allowedIPs = ["0.0.0.0/0"]; # Forward all the traffic via VPN.
            endpoint = "80.14.193.53:51820"; # Server IP and port.
            persistentKeepalive = 25; # Send keepalives every 25 seconds. Important to keep NAT tables alive.
          }
        ];
      };
    };

    sops = {
      secrets = {
        "${wgPrivateKeySopKey}" = {neededForUsers = true;};
      };
    };
  };
}
