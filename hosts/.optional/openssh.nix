{
  lib,
  config,
  ...
}: let
  extraPubKeys = lib.lists.forEach config.nindouja.user.extraSshAuthProfile (profile: builtins.readFile ../../homes/.keys/id_${profile}.pub);
in {
  services.openssh = {
    enable = lib.mkForce config.nindouja.features.ssh;
    settings = {
      PasswordAuthentication = lib.mkForce false;
      PermitRootLogin = lib.mkForce "no";
    };
  };

  users.users.${config.nindouja.user.username} = {
    # Authorize connection from:
    # - the user on this system
    # - some optional extra users
    openssh.authorizedKeys.keys = [config.nindouja.user.publicKey] ++ extraPubKeys;
  };
}
