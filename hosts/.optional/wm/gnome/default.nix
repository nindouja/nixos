{lib, ...}: {
  # Enable the X11 windowing system.
  services.xserver.enable = lib.mkForce true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = lib.mkForce true;
  services.xserver.desktopManager.gnome.enable = lib.mkForce true;
}
