{pkgs, ...}: {
  # TODO
  fonts.packages = with pkgs; [
    font-awesome
    noto-fonts-emoji
    (nerdfonts.override {fonts = ["FiraCode" "JetBrainsMono" "Iosevka"];})
  ];

  xdg.portal = {
    enable = true;
  };

  environment.systemPackages = with pkgs; [
    brightnessctl
    networkmanagerapplet
  ];

  programs = {
    hyprland = {
      enable = true;
      xwayland = {
        enable = true;
      };
      portalPackage = pkgs.xdg-desktop-portal-hyprland;
    };
  };
}
