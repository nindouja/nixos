{
  lib,
  pkgs,
  config,
  ...
}:
# Still kindof of a work in progress.
# Notes: gamemode, gamescope, mangohud and nvidia-offload are all wrapper scripts
#    * gamemode - sets priority for the process
#    * gamescope - supports for steam in compositor I believe ? Does not work ayway
#    * mangohud - display an overlay for FPS, ect (installed in home-manager)
#    * nvidia-offload - offloads the workload to the GPU
#
# Command to add to steam properties:
#   nvidia-offload gamemoderun gamescope mangohud %command%
# it's possible to add gamescope if needed. But it works without and don't work with it for me
{
  config = lib.mkIf config.nindouja.apps.steam {
    hardware.xone.enable = true; # Xbox gamepad

    environment.systemPackages = with pkgs; [
      linuxKernel.packages.linux_zen.xone # Xbox gamepad

      (pkgs.writeShellScriptBin "nvidia-offload" ''
        export __NV_PRIME_RENDER_OFFLOAD=1
        export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0
        export __GLX_VENDOR_LIBRARY_NAME=nvidia
        export __VK_LAYER_NV_optimus=NVIDIA_only
        exec "$@"
      '')
    ];
    programs = {
      gamemode.enable = true;
      gamescope.enable = true;
      steam = {
        enable = true;
        gamescopeSession.enable = true;
        remotePlay.openFirewall = false;
        dedicatedServer.openFirewall = false;
      };
    };
    nixpkgs.config.packageOverrides = pkgs: {
      steam = pkgs.steam.override {
        extraPkgs = pkgs:
          with pkgs; [
            mangohud
          ];
      };
    };
  };
}
