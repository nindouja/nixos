{
  lib,
  pkgs,
  config,
  ...
}: let
  username = config.nindouja.user.username;
  backupRepoPasswordSopKey = "users/${username}/backup_repo";
  protonPasswordSopKey = "users/${username}/proton/password";
  proton2faSopKey = "users/${username}/proton/twofactor";
in {
  # FIXME Proton backup. Currently rate limited but maybe close to objective

  config = lib.mkIf config.nindouja.features.proton-sync.enable {
    environment.systemPackages = with pkgs; [
      restic
      rclone

      (pkgs.writeShellScriptBin "rcbckp" ''
        rclone \
          "''${@}" \
          --config /etc/rclone-proton-backup.conf \
          --protondrive-password $(cat ${config.sops.secrets."${protonPasswordSopKey}".path}) \
          --protondrive-2fa $(cat ${config.sops.secrets."${proton2faSopKey}".path})
      '')

      (pkgs.writeShellScriptBin "do_backup" ''
        rcbckp ls proton_backup:
      '')
    ];

    environment.etc."rclone-proton-backup.conf".text = ''
      [proton_backup]
      type = protondrive
      username = ${config.nindouja.features.proton-sync.username}
      passwordFile = ${config.sops.secrets."${protonPasswordSopKey}".path}
      2faFile = ${config.sops.secrets."${proton2faSopKey}".path}
    '';

    services.restic.backups = {
      proton = {
        initialize = true;
        repository = "rclone:proton_backup:/Home/nindouja";
        passwordFile = config.sops.secrets."${backupRepoPasswordSopKey}".path;
        rcloneConfigFile = "/etc/rclone-proton-backup.conf";

        paths = [
          #"${config.users.users.${username}.home}/Documents"
          #"${config.users.users.${username}.home}/Downloads"
          #"${config.users.users.${username}.home}/Media"
          "${config.users.users.${username}.home}/bckp" # TODO dummy flder for test
        ];
        exclude = [
          "${config.users.users.${username}.home}/Documents/Code"
        ];

        timerConfig = {
          OnCalendar = "monday 21:00";
        };
      };
    };

    sops = {
      secrets = {
        "${protonPasswordSopKey}" = {
          owner = config.users.users.${username}.name;
          inherit (config.users.users.${username}) group;
        };
        "${proton2faSopKey}" = {
          owner = config.users.users.${username}.name;
          inherit (config.users.users.${username}) group;
        };
        "${backupRepoPasswordSopKey}" = {
          owner = config.users.users.${username}.name;
          inherit (config.users.users.${username}) group;
        };
      };
    };
  };
}
