{
  lib,
  pkgs-unstable,
  config,
  inputs,
  ...
}: {
  config = lib.mkIf config.nindouja.dev.java {
    environment.systemPackages = with pkgs-unstable; [
      openjdk

      (jetbrains.plugins.addPlugins jetbrains.idea-ultimate (import ../../resources/idea-plugins.nix))
    ];
  };
}
