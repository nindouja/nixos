{...}: {
  imports = [
    ./drivers
    ./docker.nix
    ./go.nix
    ./input-remapper.nix
    ./java.nix
    ./k3s.nix
    ./nixvim.nix
    ./ollama.nix
    ./openssh.nix
    ./pipewire.nix
    ./proton-rclone.nix
    ./rust.nix
    ./steam.nix
    ./swap.nix
    ./stylix.nix
    ./virtualisation.nix
    ./vizbab-shares.nix
    ./lan_vpn.nix
  ];
}
