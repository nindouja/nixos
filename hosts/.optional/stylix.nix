{
  lib,
  config,
  stylix,
  ...
}: {
  # FIXME conditinal stylix.nixosModules.stylix
  imports = [
    stylix.nixosModules.stylix
    ../../resources/stylix-common.nix
  ];
}
