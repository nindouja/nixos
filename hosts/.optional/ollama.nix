{
  lib,
  pkgs-unstable,
  config,
  options,
  ...
}: {
  config = lib.mkMerge [
    (lib.mkIf config.nindouja.apps.alpaca {
      environment.systemPackages = with pkgs-unstable; [
        alpaca # UI chatbot
      ];
    })
    (lib.mkIf config.nindouja.features.ollama {
      environment.systemPackages = with pkgs-unstable; [
        oterm # TUI chatbot
      ];

      services.ollama = {
        package = pkgs-unstable.ollama;
        enable = true;
        acceleration =
          if config.nindouja.features.nvidia-drivers
          then "cuda"
          else options.services.ollama.acceleration.default;
      };
    })
  ];
}
