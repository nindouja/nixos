{...}: {
  imports = [
    ./bluetooth.nix
    ./nvidia.nix
    ./openrgb.nix
  ];
}
