{
  lib,
  pkgs,
  config,
  ...
}: {
  # TODO
  config = lib.mkIf config.nindouja.features.openrgb {
    environment.systemPackages = with pkgs; [
      openrgb
      i2c-tools
    ];

    boot.kernelModules = ["i2c-dev" "i2c-piix4"];

    services.udev.extraRules = builtins.readFile ../../../resources/60-openrgb.rules;
  };
  #services.udev.extraRules = builtins.readFile (builtins.fetchurl {
  #  url = "https://openrgb.org/releases/release_0.9/60-openrgb.rules";
  #  sha256 = "sha256:0f5bmz0q8gs26mhy4m55gvbvcyvd7c0bf92aal4dsyg9n7lyq6xp";
  #});
}
