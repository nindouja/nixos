{inputs, ...}: {
  imports = [
    # Host's specific stuff
    inputs.nixos-hardware.nixosModules.asus-zephyrus-gu603h
  ];
}
