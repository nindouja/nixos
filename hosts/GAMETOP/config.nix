{
  lib,
  pkgs,
  config,
  ...
}: let
  buildBar = import ../../overlays/buildBar.nix;
in {
  config.nindouja = {
    appearance = {
      darkmode = true;
    };

    features = {
      bluetooth = true;
      wireguard = {
        enable = false; #FIXME
        publicKey = "ITVcXVAzBZE603o6BJ2WNpa8f1k30LYHMuF8lQBEKWA=";
      };
    };

    devices = {
      swap_size = 20;
      eth = "eno2";
      wifi = "wlo1";
      mouse = "device:logitech-g502-1";
      keyboard = "device:asus-keyboard-1";
      keyboardBacklight = "asus::kbd_backlight";
      screenBacklight = "intel_backlight";
      battery = "BAT0";
      monitors = [
        {
          id = "eDP-1";
          width = "2560";
          height = "1600";
          posX = "0";
          posY = "0";
          fps = "144";
          scale = "1.25";
          workspaces = [1 2 3 4 5 6 7 8];
          panel = {
            left = [
              "dashboard"
              "battery"
              "volume"
              "microphone"
              "bluetooth"
              "network"
              "windowtitle"
            ];
            middle = [
              "clock"
              "media"
            ];
            right = [
              "workspaces"
              "systray"
              "weather"
              "hypridle"
              "hyprsunset"
              "notifications"
              "power"
            ];
          };
        }
      ];
    };
  };
}
