{
  lib,
  config,
  ...
}: let
  buildBar = import ../../overlays/buildBar.nix;
in {
  config.nindouja = {
    features = {
      bluetooth = true;
      nvidia-drivers = true;
    };

    devices = {
      swap_size = 32;
      eth = "eno2";
      wifi = "wlo1";
      mouse = "logitech-usb-receiver-1";
      keyboard = "logitech-usb-receiver-keyboard-2";
      mainThermalZone = 1;
      monitors = [
        {
          id = "DP-1";
          width = "2560";
          height = "1440";
          posX = "0";
          posY = "0";
          fps = "60";
          workspaces = [1 2 3 4 5 6];
          hyprpanel = "main";
        }
        {
          id = "HDMI-A-1";
          width = "1920";
          height = "1080";
          posX = "-2120"; # Should be -1920 to allow mouse to move accross
          posY = "0";
          fps = "60";
          workspaces = [7 8 9 10 11 12];
          hyprpanel = "light";
        }
      ];
    };
  };
}
