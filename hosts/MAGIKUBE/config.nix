{
  lib,
  config,
  ...
}: {
  config.nindouja = {
    devices = {
      hasSoundSupport = false;
    };
  };
}
