{pkgs, ...}: {
  bg.path = ../wallpapers/paper-pixel-desert.png;

  font = {
    name = "Jetbrains Mono";
    package = pkgs.jetbrains-mono;
  };

  dark = {
    palette = {
      name = "selenized-dark";
    };
    cursors = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
    };
  };

  light = {
    palette = {
      name = "selenized-light";
    };
    cursors = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
    };
  };
}
