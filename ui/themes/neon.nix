{pkgs, ...}: {
  bg.path = ../wallpapers/xK.gif;

  font = {
    name = "Jetbrains Mono";
    package = pkgs.jetbrains-mono;
  };

  dark = {
    palette = {
      name = "rose-pine-moon";
    };
    cursors = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
    };
  };

  light = {
    palette = {
      name = "rose-pine-dawn";
    };
    cursors = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
    };
  };
}
