{config}: ''

  /* Main style */
  * {
    font-family: FiraCode Nerd Font, FiraCode NF, ${config.nindouja.appearance.theme.font.name};
    border: none;
    border-radius: 0;
    min-height: 0;
    font-size: 16px;
  }
  window#waybar {
    color: #${config.lib.stylix.colors.base07};
    border: none;
    background-color: transparent;
  }

  /* Box definitions */
  #battery,
  #bluetooth,
  #cpu,
  #disk,
  #idle_inhibitor,
  #memory,
  #network.1,
  #network.2,
  #pulseaudio,
  #temperature,
  #tray.horizontal,
  #user {
    /* Light box */
    margin: 8px 4px;
    padding: 2px 10px 0px 10px;
    border-radius: 8px;
    color: #${config.lib.stylix.colors.base00};
    background-color: #${config.lib.stylix.colors.base08};
  }

  tooltip,
  #custom-logo,
  #custom-playerctl.backward,
  #custom-playerctl.foward,
  #custom-playerctl.play,
  #idle_inhibitor.activated,
  #clock,
  #date,
  #group-power {
    /* Dark box */
    margin: 8px 4px;
    padding: 2px 10px 0px 10px;
    border-radius: 8px;
    color: #${config.lib.stylix.colors.base0D};
    background-color: #${config.lib.stylix.colors.base02};
  }

  #window,
  #cava.left,
  #cava.right,
  #custom-playerlabel,
  #workspaces.horizontal {
    /* No box*/
    margin: 8px 4px;
    padding: 2px 10px 0px 10px;
    border-radius: 8px;
    color: #${config.lib.stylix.colors.base0D};
    background-color: transparent;
    border: 1px solid transparent;
  }



  /* Tooltips */
  tooltip {
    padding: 15px;
    border-radius: 15px;
  }
  tooltip label {
    padding: 5px;
  }
  label:focus {
    background-color: #${config.lib.stylix.colors.base07};
  }


  /* Module overides */
  #window {
    font-size: 14px;
    background-color: rgba(0,0,0, 0.25);
  }

  #custom-logo {
    font-size: 24px;
    padding-top: 2px;
    padding-right: 12px;
    margin: 8px 8px 8px 8px;
    border: 1px solid transparent;
  }

  #clock, #date{
    border: 1px solid transparent;
    font-weight: bolder;
    font-size: 20px;
    margin: 8px 8px 8px 8px;
    padding-top: 4px;
  }

  #tray {
    margin: 7px 8px;
  }
  #tray>.passive {
    -gtk-icon-effect: dim;
  }
  #tray>.needs-attention {
    -gtk-icon-effect: highlight;
    background-color: #${config.lib.stylix.colors.base0E};
  }

  #idle_inhibitor {
    padding: 0 10px;
  }
  #idle_inhibitor.activated{
    color: #${config.lib.stylix.colors.base08};
  }
  #custom-power_menu{
    color: #f54251;
    margin: 0px 3px 0px 3px;
  }
  #custom-power{
    color: #f54251;
  }
  #custom-reboot{
    color: #f54251;
  }
  #custom-logout{
    color: #fa9725;
  }
  #custom-lock{
    color: #e8e343;
  }
  .drawer-child{
    margin: 3px 3px 3px 3px;
  }

  #privacy-item{
    color: #fa9725;
  }

  #workspaces.horizontal {
    font-size: 30px;
    background-color: #${config.lib.stylix.colors.base05};
  }
  #workspaces button {
    padding: 2px 4px 2px 2px;
    margin: 3px 3px 3px 3px;
    border-radius: 20px;
    border-color: #${config.lib.stylix.colors.base01};
    color: #${config.lib.stylix.colors.base07};
    box-shadow: none;
    text-shadow: none;
    background: none;
    transition: none;
    font-size: 30px;
  }
  #workspaces button:hover {
    padding: 0 3px;
    color: #${config.lib.stylix.colors.base0B};
  }
  #workspaces button.active {
    font-weight: bolder;
    color: #${config.lib.stylix.colors.base08};
  }
  #workspaces button.urgent {
    color: #${config.lib.stylix.colors.base09};
    background-color: #${config.lib.stylix.colors.base00};
    animation-name: blink;
    animation-duration: 0.5s;
    animation-timing-function: linear;
    animation-iteration-count: 20;
    animation-direction: alternate;
  }
  .modules-left>widget:first-child>#workspaces {
    /* If workspaces is the leftmost module, omit left margin */
    margin-left: 0;
  }
  .modules-right>widget:last-child>#workspaces {
    /* If workspaces is the rightmost module, omit right margin */
    margin-right: 0;
  }


  /* Special states */
  #pulseaudio.muted {
    color: #${config.lib.stylix.colors.base09};
  }
  #battery.charging,
  #battery.plugged {
    color: #${config.lib.stylix.colors.base0C};
  }
  #battery.critical:not(.charging) {
    background-color: #${config.lib.stylix.colors.base09};
    color: #${config.lib.stylix.colors.base08};
    animation-name: blink;
    animation-duration: 0.5s;
    animation-timing-function: linear;
    animation-iteration-count: infinite;
    animation-direction: alternate;
  }
  @keyframes blink {
    to {
      color: #${config.lib.stylix.colors.base0F};
    }
  }
''
