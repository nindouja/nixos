{config, ...}: {
  framerate = 60;
  autosens = 0;
  sensitivity = 5;
  bars = 16;
  lower_cutoff_freq = 50;
  higher_cutoff_freq = 10000;
  method = "pipewire";
  source = "auto";
  stereo = true;
  reverse = false;
  bar_delimiter = 0;
  monstercat = false;
  waves = false;
  input_delay = 2;
  format-icons = [
    "<span foreground='#${config.lib.stylix.colors.base01}'>▁</span>"
    "<span foreground='#${config.lib.stylix.colors.base01}'>▂</span>"
    "<span foreground='#${config.lib.stylix.colors.base0A}'>▃</span>"
    "<span foreground='#${config.lib.stylix.colors.base0B}'>▄</span>"
    "<span foreground='#${config.lib.stylix.colors.base0B}'>▅</span>"
    "<span foreground='#${config.lib.stylix.colors.base0C}'>▆</span>"
    "<span foreground='#${config.lib.stylix.colors.base0C}'>▇</span>"
    "<span foreground='#${config.lib.stylix.colors.base03}'>█</span>"
  ];
}
