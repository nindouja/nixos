{...}: {
  format = " {}";
  max-length = 75;
  separate-outputs = true;
}
