{
  formatIcons,
  workspaces,
  ...
}: {
  format = "{icon}";
  all-outputs = false;
  on-click = "activate";
  format-window-separator = "-";
  format-icons = formatIcons;
  persistent-workspaces = {
    "*" = workspaces;
  };
}
