{config, ...}: {
  device = config.nindouja.devices.screenBacklight;
  format = "{icon}";
  format-icons = ["󱩎" "󱩏" "󱩐" "󱩑" "󱩒" "󱩓" "󱩔" "󱩕" "󱩖" "󰛨"];
  tooltip = true;
  format-alt = "<small>{percent}%</small>";
  on-scroll-up = "brightnessctl set 1%+";
  on-scroll-down = "brightnessctl set 1%-";
  smooth-scrolling-threshold = "2400";
  tooltip-format = "Brightness {percent}%";
}
