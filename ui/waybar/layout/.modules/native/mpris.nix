{...}: {
  format = "{player_icon} {title}";
  format-paused = " {status_icon} <i>{title}</i>";
  max-length = 80;
  player-icons = {
    default = "▶";
    mpv = "🎵"; #TODO
  };
  status-icons = {
    paused = "⏸";
  };
}
