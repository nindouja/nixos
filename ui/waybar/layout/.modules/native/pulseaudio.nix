{...}: {
  scroll-step = 1;
  format = "{volume}% {icon} {format_source}";
  format-bluetooth = "{volume}% {icon}  {format_source}";
  format-bluetooth-muted = "󰸈 {icon}  {format_source}";
  format-muted = "󰸈 {format_source}";
  format-source = "";
  format-source-muted = "";
  format-icons = {
    headphone = "";
    hands-free = "";
    headset = "";
    phone = "";
    portable = "";
    car = "";
    default = ["" "" ""];
  };
  on-click = "amixer set Master toggle";
  on-click-right = "amixer set Capture toggle";
  on-double-click = "pypr toggle volume";
}
