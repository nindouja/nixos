{config, ...}: {
  interval = 5;
  format = "󰘚 {usage}%";
  format-alt = "{usage}% 󰘚 {icon0}{icon1}{icon2}{icon3}{icon4}{icon5}{icon6}{icon7}";
  format-icons = [
    "<span color='#${config.lib.stylix.colors.base05}'>▁</span>"
    "<span color='#${config.lib.stylix.colors.base05}'>▂</span>"
    "<span color='#${config.lib.stylix.colors.base05}'>▃</span>"
    "<span color='#${config.lib.stylix.colors.base0A}'>▄</span>"
    "<span color='#${config.lib.stylix.colors.base0A}'>▅</span>"
    "<span color='#${config.lib.stylix.colors.base09}'>▆</span>"
    "<span color='#${config.lib.stylix.colors.base09}'>▇</span>"
    "<span color='#${config.lib.stylix.colors.base08}'>█</span>"
  ];
}
