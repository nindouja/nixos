{config, ...}: {
  interval = 10;
  interface = "${config.nindouja.devices.wifi}";
  format-wifi = "<small>{bandwidthDownBytes}  | {bandwidthUpBytes} </small>";
  tooltip-format-wifi = "{ipaddr}/{cidr}  |  {essid} ({signalStrength}%)";
  format-disconnected = "󱛅";
  format-icon = ["󰤯 " "󰤟 " "󰤢 " "󰤥 " "󰤨 "];
  on-double-click = "kitty nmtui";
}
