{...}: {
  interval = 30;
  format = " {free}";
  format-alt = "{path} : {used}/{total}";
  path = "/";
}
