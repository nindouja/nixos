{...}: {
  format = "{user}";
  interval = 60;
  height = 15;
  width = 15;
  icon = true;
}
