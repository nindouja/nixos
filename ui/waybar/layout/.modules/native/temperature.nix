{config, ...}: {
  thermal-zone = config.nindouja.devices.mainThermalZone;
  interval = 10;
  max-length = 10;
  critical-threshold = 80;
  format = " {temperatureC}°C";
}
