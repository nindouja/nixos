{config, ...}: {
  interval = 1;
  timezone = config.nindouja.l10n.timezone;
  format = "{:%d %b - %H:%M}";
  format-alt = "{:%d %b %y - %H:%M:%S}";
  tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
}
