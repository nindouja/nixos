{config, ...}: {
  interval = 10;
  interface = "${config.nindouja.devices.eth}";
  format-ethernet = "󰈁";
  format-alt = "<small>{bandwidthDownBytes}  | {bandwidthUpBytes} </small>";
  tooltip-format-ethernet = "{ipaddr}/{cidr}";
  format-disconnected = "󰈂";
}
