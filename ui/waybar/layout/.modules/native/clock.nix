{config, ...}: {
  interval = 1;
  timezone = config.nindouja.l10n.timezone;
  format = "{:%H:%M}";
  format-alt = "{:%d %b %Y - %H:%M}";
  tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
}
