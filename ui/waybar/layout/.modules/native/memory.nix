{...}: {
  tooltip = true;
  interval = 10;
  format = " {}%";
  format-alt = " {used:0.1f}/{total:0.1f}GB";
  tooltip-format = "{used:0.1f}G/{total:0.1f}GB";
}
