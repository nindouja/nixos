{...}: {
  drawer = {
    transition-duration = 300;
    click-to-reveal = false;
  };
  modules = [
    "custom/power_menu"
    "custom/power"
    "custom/reboot"
    "custom/logout"
    "custom/lock"
  ];
}
