{...}: {
  format = "<span>󰎈 {}</span>";
  return-type = "json";
  max-length = 40;
  exec = "playerctl -a metadata --format '{\"text\": \"{{artist}} - {{markup_escape(title)}}\", \"tooltip\": \"{{playerName}} : {{markup_escape(title)}}\", \"alt\": \"{{status}}\", \"class\": \"{{status}}\"}' -F";
  on-click = "playerctl play-pause";
  on-scroll-up = "playerctl volume .05+";
  on-scroll-down = "playerctl volume .05-";
}
