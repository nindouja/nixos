{...}: {
  format = "{}";
  exec = ''echo "󰨈 󱣻 󱩡" | tr ' ' '\n' | shuf | awk 'NR==1' '';
  interval = 5;
  on-click = "kill_overlays && app_launcher";
  on-click-right = "kill_overlays && clipboard_manager";
  on-click-middle = "kitty --hold fastfetch";
}
