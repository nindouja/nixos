{...}: {
  format = " 󰙡 ";
  on-click = "playerctl next";
  on-scroll-up = "playerctl volume .05+";
  on-scroll-down = "playerctl volume .05-";
}
