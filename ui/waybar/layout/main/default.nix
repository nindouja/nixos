{
  config,
  formatIcons,
  workspaces,
  ...
}: let
  add_if = bool: name:
    if bool
    then [name]
    else [];
in {
  modules-left =
    [
      "custom/logo"
    ]
    ++ (add_if (config.nindouja.devices.screenBacklight != "none") "backlight")
    ++ (add_if (config.nindouja.devices.battery != "none") "battery")
    ++ [
      "pulseaudio"
      "bluetooth"
    ]
    ++ (add_if (config.nindouja.devices.eth != "none") "network#1")
    ++ (add_if (config.nindouja.devices.wifi != "none") "network#2")
    ++ [
      "hyprland/window"
    ];
  modules-center = [
    "clock"
  ];
  modules-right =
    [
      "hyprland/workspaces"
    ]
    ++ [
      "tray"
      "idle_inhibitor"
      "group/group-power"
    ];

  "custom/logo" = import ../.modules/custom/logo.nix {inherit config;};
  "backlight" = import ../.modules/native/backlight.nix {inherit config;};
  "battery" = import ../.modules/native/battery.nix {inherit config;};
  "pulseaudio" = import ../.modules/native/pulseaudio.nix {inherit config;};
  "bluetooth" = import ../.modules/native/bluetooth.nix {inherit config;};
  "network#1" = import ../.modules/native/network-1.nix {inherit config;};
  "network#2" = import ../.modules/native/network-2.nix {inherit config;};
  "hyprland/window" = import ../.modules/third-party/hyprland-window.nix {inherit config;};

  "clock" = import ../.modules/native/date.nix {inherit config;};

  "hyprland/workspaces" = import ../.modules/third-party/hyprland-workspaces.nix {inherit workspaces formatIcons config;};
  "tray" = import ../.modules/native/tray.nix {inherit config;};
  "idle_inhibitor" = import ../.modules/native/idle_inhibitor.nix {inherit config;};
  "group/group-power" = import ../.modules/custom/power_menu.nix {inherit config;};

  "custom/power_menu" = {
    format = "";
    tooltip = false;
  };
  "custom/power" = {
    format = "";
    tooltip = false;
    on-click = "shutdown now";
  };
  "custom/reboot" = {
    format = "󰜉";
    tooltip = false;
    on-click = "reboot";
  };
  "custom/logout" = {
    format = "󰗼";
    tooltip = false;
    on-click = "hyprctl dispatch exit";
  };
  "custom/lock" = {
    format = "󰍁";
    tooltip = false;
    on-click = "hyprlock";
  };
}
