{
  config,
  formatIcons,
  workspaces,
  ...
}: {
  modules-left = [
    "custom/logo"
    "hyprland/window"
  ];
  modules-center = [
    "hyprland/workspaces"
  ];
  modules-right = [
    "clock"
  ];

  "custom/logo" = import ../.modules/custom/logo.nix {inherit config;};
  "user" = import ../.modules/native/user.nix {inherit config;};
  "hyprland/workspaces" = import ../.modules/third-party/hyprland-workspaces.nix {inherit workspaces formatIcons config;};
  "hyprland/window" = import ../.modules/third-party/hyprland-window.nix {inherit config;};
  "clock" = import ../.modules/native/clock.nix {inherit config;};
}
