{config, ...}: {
  modules-left = [
  ];
  modules-center = [
    "clock"
  ];
  modules-right = [
  ];

  "clock" = import ../.modules/native/clock.nix {inherit config;};
}
