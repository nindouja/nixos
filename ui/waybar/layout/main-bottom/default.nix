{config, ...}: {
  modules-left = [
    "custom/logo"
    "privacy"
    "custom/playerlabel"
  ];
  modules-center = [
    "custom/playerctl#backward"
    "custom/playerctl#play"
    "custom/playerctl#foward"
  ];
  modules-right = [
    "temperature"
    "disk"
    "memory"
    "cpu"
    "clock"
  ];

  "custom/logo" = import ../.modules/custom/logo.nix {inherit config;};
  "privacy" = import ../.modules/native/privacy.nix {inherit config;};
  "custom/playerlabel" = import ../.modules/custom/playerlabel.nix {inherit config;};

  "custom/playerctl#backward" = import ../.modules/custom/playerctl-backward.nix {inherit config;};
  "custom/playerctl#play" = import ../.modules/custom/playerctl-play.nix {inherit config;};
  "custom/playerctl#foward" = import ../.modules/custom/playerctl-foward.nix {inherit config;};

  "temperature" = import ../.modules/native/temperature.nix {inherit config;};
  "disk" = import ../.modules/native/disk.nix {inherit config;};
  "memory" = import ../.modules/native/memory.nix {inherit config;};
  "cpu" = import ../.modules/native/cpu.nix {inherit config;};
  "clock" = import ../.modules/native/clock.nix {inherit config;};
}
