{config, ...}: let
  light_fg = config.lib.stylix.colors.base00;
  light_bg = config.lib.stylix.colors.base08;
  dark_fg = config.lib.stylix.colors.base0D;
  dark_bg = config.lib.stylix.colors.base02;
in {
  theme = {
    bar = {
      border_radius = "0.4em";
      dropdownGap = "2.9em";
      enableShadow = false;
      floating = true;
      label_spacing = "0.5em";
      layer = "top";
      location = "top";
      margin_bottom = "0em";
      margin_sides = "0.5em";
      margin_top = "0.5em";
      opacity = 100;
      outer_spacing = "1.6em";
      scaling = 100;
      shadow = "0px 1px 2px 1px #16161e";
      shadowMargins = "0px 0px 4px 0px";
      transparent = false;

      border = {
        location = "none";
        width = "0.15em";
      };
      buttons = {
        background_hover_opacity = 100;
        background_opacity = 100;
        battery = {
          enableBorder = false;
          spacing = "0.5em";
        };
        bluetooth = {
          enableBorder = false;
          spacing = "0.5em";
        };
        borderSize = "0.03em";
        clock = {
          enableBorder = false;
          spacing = "0.5em";
        };
        dashboard = {
          enableBorder = true;
          spacing = "0.5em";
        };
        enableBorders = false;
        innerRadiusMultiplier = "0.4";
        media = {
          enableBorder = false;
          spacing = "0.5em";
        };
        modules = {
          cpu = {
            enableBorder = false;
            spacing = "0.5em";
          };
          cpuTemp = {
            enableBorder = false;
            spacing = "0.5em";
          };
          hypridle = {
            enableBorder = false;
            spacing = "0.45em";
          };
          hyprsunset = {
            enableBorder = false;
            spacing = "0.45em";
          };
          kbLayout = {
            enableBorder = false;
            spacing = "0.45em";
          };
          microphone = {
            enableBorder = false;
            spacing = "0.45em";
          };
          netstat = {
            enableBorder = false;
            spacing = "0.45em";
          };
          power = {
            enableBorder = false;
            spacing = "0.45em";
          };
          ram = {
            enableBorder = false;
            spacing = "0.45em";
          };
          storage = {
            enableBorder = false;
            spacing = "0.45em";
          };
          submap = {
            enableBorder = false;
            spacing = "0.45em";
          };
          updates = {
            enableBorder = false;
            spacing = "0.45em";
          };
          weather = {
            enableBorder = false;
            spacing = "0.45em";
          };
        };
        monochrome = false;
        network = {
          enableBorder = false;
          spacing = "0.5em";
        };
        notifications = {
          enableBorder = false;
          spacing = "0.5em";
        };
        opacity = 100;
        padding_x = "0.7rem";
        padding_y = "0.2rem";
        radius = "0.3em";
        spacing = "0.25em";
        style = "default";
        systray = {
          enableBorder = true;
          spacing = "0.5em";
        };
        volume = {
          enableBorder = false;
          spacing = "0.5em";
        };
        windowtitle = {
          enableBorder = true;
          spacing = "0.5em";
        };
        workspaces = {
          enableBorder = true;
          fontSize = "1.2em";
          numbered_active_highlight_border = "0.2em";
          numbered_active_highlight_padding = "0.2em";
          numbered_inactive_padding = "0.2em";
          pill = {
            active_width = "12em";
            height = "4em";
            radius = "1.9rem * 0.6";
            width = "4em";
          };
          smartHighlight = true;
          spacing = "0.5em";
        };
        y_margins = "0.4em";
      };
      menus = {
        border = {
          radius = "0.7em";
          size = "0.13em";
        };
        buttons = {
          radius = "0.4em";
        };
        card_radius = "0.4em";
        enableShadow = false;
        menu = {
          battery = {
            scaling = 100;
          };
          bluetooth = {
            scaling = 100;
          };
          clock = {
            scaling = 100;
          };
          dashboard = {
            confirmation_scaling = 100;
            profile = {
              radius = "0.4em";
              size = "8.5em";
            };
            scaling = 75;
          };
          media = {
            card = {
              tint = 85;
            };
            scaling = 100;
          };
          network = {
            scaling = 100;
          };
          notifications = {
            height = "75em";
            pager = {
              show = true;
            };
            scaling = 90;
            scrollbar = {
              radius = "0.2em";
              width = "0.35em";
            };
          };
          power = {
            radius = "0.4em";
            scaling = 90;
          };
          volume = {
            scaling = 100;
          };
        };
        monochrome = false;
        opacity = 100;
        popover = {
          radius = "0.4em";
          scaling = 100;
        };
        progressbar = {
          radius = "0.3rem";
        };
        scroller = {
          radius = "0.7em";
          width = "0.25em";
        };
        shadow = "0px 0px 3px 1px #16161e";
        shadowMargins = "5px 5px";
        slider = {
          progress_radius = "0.3rem";
          slider_radius = "0.3rem";
        };
        switch = {
          radius = "0.2em";
          slider_radius = "0.2em";
        };
        text = "#FF0000";
        tooltip = {
          radius = "0.3em";
        };
      };
    };
    matugen = false;
    matugen_settings = {
      contrast = 0;
      mode = "dark";
      scheme_type = "tonal-spot";
      variation = "standard_1";
    };
    font = {
      name = "JetBrainsMono Nerd Font";
      size = "1.2rem";
      weight = 600;
      label = "JetBrainsMono Nerd Font Thin";
    };
    notification = {
      border_radius = "0.6em";
      enableShadow = false;
      opacity = 100;
      scaling = 100;
      shadow = "0px 1px 2px 1px #16161e";
      shadowMargins = "4px 4px";
    };
    osd = {
      active_monitor = true;
      border = {
        size = "0.1em";
      };
      duration = 1000;
      enable = true;
      enableShadow = false;
      location = "right";
      margins = "0px 20px 0px 0px";
      monitor = 0;
      muted_zero = false;
      opacity = 100;
      orientation = "vertical";
      radius = "0.4em";
      scaling = 100;
      shadow = "0px 0px 3px 2px #16161e";
    };
    tooltip = {
      scaling = 100;
    };
  };
}
