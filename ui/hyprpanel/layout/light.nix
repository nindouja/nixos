{config, ...}: {
  left = [
    "dashboard"
    "windowtitle"
  ];
  middle = [
    "clock"
    "media"
  ];
  right = [
    "workspaces"
  ];
}
