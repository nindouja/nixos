{config, ...}: let
  add_if = bool: name:
    if bool
    then [name]
    else [];
in {
  left =
    [
      "dashboard"
    ]
    ++ (add_if (config.nindouja.devices.battery != "none") "battery")
    ++ [
      "volume"
      "microphone"
      "bluetooth"
      "network"
      "windowtitle"
    ];
  middle = [
    "clock"
    "media"
  ];
  right = [
    "workspaces"
    "systray"
    "weather"
    "hypridle"
    "hyprsunset"
    "notifications"
    "power"
  ];
}
