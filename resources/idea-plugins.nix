[
  # "github-copilot" # https://plugins.jetbrains.com/plugin/17718-github-copilot
  "continue" # https://plugins.jetbrains.com/plugin/22707-continue
  "dot-language" # https://plugins.jetbrains.com/plugin/10312-dot-language
  "better-direnv" # https://plugins.jetbrains.com/plugin/19275-better-direnv
  "ansi-highlighter-premium" # https://plugins.jetbrains.com/plugin/9707-ansi-highlighter-premium
  "nixidea" # https://plugins.jetbrains.com/plugin/8607-nixidea
  "mermaid" # https://plugins.jetbrains.com/plugin/20146-mermaid
  "csv-editor" # https://plugins.jetbrains.com/plugin/10037-csv-editor
  "nix-lsp" # https://plugins.jetbrains.com/plugin/25594-nix-lsp
  #TODO not in rust rover "cognitivecomplexity" # https://plugins.jetbrains.com/plugin/12024-cognitivecomplexity
  "string-manipulation" # https://plugins.jetbrains.com/plugin/2162-string-manipulation
  "mermaid-chart" # https://plugins.jetbrains.com/plugin/23043-mermaid-chart
  "code-complexity" # https://plugins.jetbrains.com/plugin/21667-code-complexity
  "grep-console" # https://plugins.jetbrains.com/plugin/7125-grep-console
  "key-promoter-x" # https://plugins.jetbrains.com/plugin/9792-key-promoter-x
  #TODO pull request "rainbow-brackets-lite" # https://plugins.jetbrains.com/plugin/20710-rainbow-brackets-lite--free-and-opensource
  "rainbow-csv" # https://plugins.jetbrains.com/plugin/12896-rainbow-csv
  "randomness" # https://plugins.jetbrains.com/plugin/9836-randomness
  "gittoolbox" # https://plugins.jetbrains.com/plugin/7499-gittoolbox
  "wakatime" # https://plugins.jetbrains.com/plugin/7425-wakatime
  "extra-ide-tweaks" # https://plugins.jetbrains.com/plugin/23927-extra-ide-tweaks
  #TODO not free"extra-toolwindow-colorful-icons" # https://plugins.jetbrains.com/plugin/16604-extra-toolwindow-colorful-icons
  #TODO not free"extra-icons" # https://plugins.jetbrains.com/plugin/11058-extra-icons
  #TODO not free"extra-icons" # https://plugins.jetbrains.com/plugin/11058-extra-icons
  "extra-tools-pack" # https://plugins.jetbrains.com/plugin/24559-extra-tools-pack
  "mario-progress-bar" # https://plugins.jetbrains.com/plugin/14708-mario-progress-bar
  "ferris" # https://plugins.jetbrains.com/plugin/21551-ferris
  "handlebars-mustache" # https://plugins.jetbrains.com/plugin/6884-handlebars-mustache
  "file-watchers" # https://plugins.jetbrains.com/plugin/7177-file-watchers
  "indent-rainbow" # https://plugins.jetbrains.com/plugin/13308-indent-rainbow
  "codeglance-pro" # https://plugins.jetbrains.com/plugin/18824-codeglance-pro
  "gitlab" # https://plugins.jetbrains.com/plugin/22857-gitlab
  "toml" # https://plugins.jetbrains.com/plugin/8195-toml
  "dev-containers" # https://plugins.jetbrains.com/plugin/21962-dev-containers
  "-env-files" # https://plugins.jetbrains.com/plugin/9525--env-files-support
  "gerry-themes" # https://plugins.jetbrains.com/plugin/18922-gerry-themes
  "markdtask" # https://plugins.jetbrains.com/plugin/26084-markdtask
  "ideavim" # https://plugins.jetbrains.com/plugin/164-ideavim
  "developer-tools" # https://plugins.jetbrains.com/plugin/21904-developer-tools
]
