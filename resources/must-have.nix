{
  pkgs,
  pkgs-unstable,
  inputs,
  ...
}: {
  must-have-pkgs =
    (with pkgs; [
      fractal
      element-desktop

      # Custom script
      inputs.ninja.outputs.packages.${system}.ninja

      # Dotfiles maintenance
      nh # Better rebuild command for nix and home-maanger
      alejandra # Format nix code
      deadnix # Remove dead code in nix files
      age # Encrypt and decrypt secrets
      sops # Integrates encrypted secrets into nix config
      ssh-to-age # Utilities to convert ssh keys to age key
      home-manager # nix declarative magic for home configurations

      # Real must have
      jq # easily work with JSON
      yq # easily work with YAML
      git # backup and access files remotely easily
      k9s # Administrate k8s cluster in style
      nixos-generators # TODO generate isos
      unzip # work with archive files
      vim # text editor
      wev # debug char code from kb/mouse input
      wget # get content over http
      unzip

      # Lazy stuff
      thefuck # Correct last command
      tldr # Lazy manpage

      # Fancy stuff
      bat # Better cat command
      bat-extras.prettybat # Better cat command
      eza # Better ls command
      fd # Better find command
      diff-so-fancy # Batter foramt for git diff output
      fzf # Fuzzy finder
      fastfetch # fancy distribution info in a CLI
      onefetch # fancy Git repo info in a CLI

      # TUIs
      lazygit
      bottom # top/htop
      tenki # Clock
      yazi # File manager
      openapi-tui
      gping
      #TODO waiting for inclusion in nixpkgs: oatmeal

      # Uncommon tools, worth to have at hand
      imagemagick
      devenv
      mermaid-cli
    ])
    ++ (with pkgs-unstable; [
      # TUIs
      serie # git graph
    ]);

  must-have-aliases = {
    # Real aliases
    "cc" = "clear";
    ".." = "cd ..";
    "d" = "docker";
    "dc" = "docker compose";
    ".dsh" = "docker exec -it $1 sh";
    ".dbash" = "docker exec -it $1 bash";
    "codium" = "codium --enable-features=UseOzonePlatform,WaylandWindowDecorations --ozone-platform-hint=auto";
    "code" = "codium --enable-features=UseOzonePlatform,WaylandWindowDecorations --ozone-platform-hint=auto";
    ".dot" = "code ~/.dotfiles";
    "dot" = "cd ~/.dotfiles && onefetch";

    # Commands replacement
    "ll" = "eza --icons -l -T -L=1";
    "l" = "eza --icons -l -T -L=1 -a";
    "c" = "bat";
    "pc" = "prettybat";
    "icat" = "kitten icat";
  };
}
