{
  lib,
  pkgs,
  stylix,
  config,
  ...
}: let
  theme_variant =
    if config.nindouja.appearance.darkmode
    then config.nindouja.appearance.theme.dark
    else config.nindouja.appearance.theme.light;
in {
  stylix = lib.mkMerge [
    (lib.mkIf (theme_variant.palette.name != "") {
      # Set default palette if provided
      base16Scheme = "${pkgs.base16-schemes}/share/themes/${theme_variant.palette.name}.yaml";
    })
    (lib.mkIf (lib.hasAttr "yaml" theme_variant.palette) {
      # Set some overrides if provided
      override = theme_variant.palette.yaml;
    })
    {
      autoEnable = true;

      polarity =
        if config.nindouja.appearance.darkmode
        then "dark"
        else "light";

      image = (
        if (config.nindouja.appearance.theme.bg.url != "")
        then
          pkgs.fetchurl {
            url = config.nindouja.appearance.theme.bg.url;
            sha256 = config.nindouja.appearance.theme.bg.sha256;
          }
        else config.nindouja.appearance.theme.bg.path
      );

      fonts = {
        monospace = {
          name = config.nindouja.appearance.theme.font.name;
          package = config.nindouja.appearance.theme.font.package;
        };
        serif = {
          name = config.nindouja.appearance.theme.font.name;
          package = config.nindouja.appearance.theme.font.package;
        };
        sansSerif = {
          name = config.nindouja.appearance.theme.font.name;
          package = config.nindouja.appearance.theme.font.package;
        };
        emoji = {
          name = "Noto Color Emoji";
          package = pkgs.noto-fonts-emoji-blob-bin;
        };
        sizes = {
          desktop = lib.mkDefault 15;
          applications = lib.mkDefault 10;
          terminal = lib.mkDefault 10;
          popups = lib.mkDefault 5;
        };
      };

      opacity = {
        terminal = lib.mkDefault 0.90;
        applications = lib.mkDefault 0.90;
        popups = lib.mkDefault 0.50;
        desktop = lib.mkDefault 0.90;
      };
    }
  ];
}
