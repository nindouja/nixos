{pkgs, ...}: {
  opts = {
    number = true;
    relativenumber = true;
    shiftwidth = 2;
  };
  globals = {
    mapleader = " ";
  };

  extraPlugins = with pkgs.vimPlugins; [
    vim-nix # Nix language support -
    comment-nvim # Commenting plugin - https://github.com/numToStr/Comment.nvim
  ];

  keymaps = [
    {
      action = "<cmd>Telescope find_files <CR>";
      key = "<C-b>"; # this line is changed
      mode = "n";
      options = {
        desc = "Toggle Tree View.";
      };
    }
  ];

  plugins = {
    lightline.enable = true; # Line at bottom of editor - https://github.com/itchyny/lightline.vim
    telescope.enable = true; # File fuzzy finder - https://github.com/nvim-telescope/telescope.nvim
    web-devicons.enable = true; # Required for teelescope
    treesitter.enable = true; # Syntax tree - https://github.com/tree-sitter/tree-sitter
    luasnip.enable = true; # Utilities - https://github.com/L3MON4D3/LuaSnip
    lsp.enable = true; # Language server -
    cmp .enable = true; # Auto completion   - https://github.com/hrsh7th/nvim-cmp
    obsidian.enable = true;

    lsp = {
      servers = {
        rust_analyzer = {
          enable = true;
          installCargo = true;
          installRustc = true;
        };
      };
    };
  };

  extraConfigLua = ''
    -- Print a little welcome message when nvim is opened!
    print("Hello world!")
  '';
}
